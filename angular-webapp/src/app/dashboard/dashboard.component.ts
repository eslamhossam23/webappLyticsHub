import {AfterViewInit, Component, OnInit} from '@angular/core';
import {WebsocketService} from "../websocket.service";
import {UserService} from "../user.service";

declare var Chart: any;
declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {


  // public categories = ['technologyPenetration', 'trafficPenetration', 'throughput','OTT', 'mobility', 'mobileCompatibility', 'coverage'];
  // public categoriesHumanNames = ['Technology Penetration', 'Traffic Penetration', 'Throughput','OTT', 'Mobility', 'Mobile Compatibility', 'Coverage'];
  // public subCategories = [["4G", "3G", "2G"], ["4G", "3G", "2G"], ["4G", "3G", "2G"], ["4G", "3G", "2G"] ,["4G", "3G", "2G"], ["4G", "3G", "2G"], ["4G", "3G", "2G"]];
  public categories = [];
  public categoriesHumanNames = [];
  public subCategories = [];
  public subCategoriesHumanNames = [];
  private response;
  private intervalID;
  private queries = [];

  constructor(private userService: UserService, private websocketService: WebsocketService) {
    this.sendQuery();
  }


  ngOnInit() {
    this.websocketService.change.subscribe(message => {
      if(Object.keys(message)[0].includes("DB_")){
        this.parseCategoriesFromServer(message);
        this.response = message;
        clearInterval(this.intervalID);

        var keys = Object.keys(this.response);

        for(var i = 0; i < keys.length; i++){
          var data = this.response[keys[i]];
          var dataSets = {};
          for(var x = 0; x < data[0].length; x++){
            var key = data[0][x];
            dataSets[key] = [];
          }
          for(var x = 1; x < data.length; x++){
            var values = data[x];
            for(var j = 0; j < values.length; j++){
              dataSets[Object.keys(dataSets)[j]].push(values[j]);
            }
          }
          var splitArray = keys[i].split("_");
          if(splitArray.length == 3){
            splitArray[3] = "All";
          }
          var category = splitArray[2].charAt(0).toLowerCase() + splitArray[2].substr(1, splitArray[2].length);
          var id = splitArray[3] + category + "Chart";
          var categoryIndex = this.categories.indexOf(category);
          var subCategoryIndex = this.subCategories[categoryIndex].indexOf(splitArray[3]);
          var subCategory = this.subCategoriesHumanNames[categoryIndex][subCategoryIndex];
          var title = this.categoriesHumanNames[categoryIndex] + " " + subCategory;
          this.queries.push([id, dataSets, title]);
        }


        setTimeout(()=>{
          for(var i = 0; i < this.queries.length; i++){
            this.drawChart(this.queries[i][0], this.queries[i][1], this.queries[i][2]);
          }
          // $('#' + this.categories[0] + "-tab").click();
          // $('#' + this.subCategories[0][0] + this.categories[0] + "Tab").click();
          this.adjustHeight();
        }, 1000);


        console.log(this.response);



      }
    });


  }

  ngAfterViewInit(){
    this.adjustHeight();
    $(window).on("resize",  () => {
      this.adjustHeight();
    });

  }

  sendQuery(){
    try {
      this.intervalID = setInterval(() => {
        var query;
        var operator = this.userService.getOperator();
        // if((<any>operator) instanceof Array){
        //   query = {"dashboard": [ operator ] };
        //   console.log("Case 1");
        // }else{
        query = {"dashboard": [ [operator] ] };
        // console.log("Case 2");
        // }
        this.websocketService.send(JSON.stringify(query));
        console.log(query);
      }, 1000);
    } catch (e){
      console.log("Couldn't send query.");
    }
  }

  parseCategoriesFromServer(message){
    var keys = Object.keys(message);
    for(var i = 0; i < keys.length; i++){
      var result = keys[i].split("_");
      if(result.length == 3){
        result[3] = "All";
      }
      var categoryFromServer = result[2];
      var category = categoryFromServer.charAt(0).toLowerCase() + categoryFromServer.substr(1, categoryFromServer.length);
      // console.log(result);
      if(!this.categoryExists(category)){
        this.categories.push(category);
        this.categoriesHumanNames.push(categoryFromServer.replace("-", " "));
        this.subCategories.push([]);
        this.subCategories[this.subCategories.length - 1].push(result[3]);
        this.subCategoriesHumanNames.push([]);
        this.subCategoriesHumanNames[this.subCategoriesHumanNames.length - 1].push(result[3].replace("-", " ").replace("lev", "Level").replace("qual", "Quality"));
        continue;
      }
      for(var j = 0; j < this.categories.length; j++){
        if(this.categories[j] === category){
          this.subCategories[j].push(result[3]);
          this.subCategoriesHumanNames[j].push(result[3].replace("-", " ").replace("lev", "Level").replace("qual", "Quality"));
          break;
        }
      }
    }
    // console.log(this.subCategories);
    // console.log(this.subCategoriesHumanNames);
  }

  categoryExists(category): boolean{
    for(var i = 0; i < this.categories.length; i++){
      if(this.categories[i] === category){
        return true;
      }
    }
    return false;
  }




  dataSetStacked(label, data, color): object{
    return {
      label: label,
      backgroundColor: color,
      data: data
    }
  }

  dataSetLine(label, data, color): object{
    return {
      label: label,
      backgroundColor: color,
      borderColor: color,
      data: data,
      fill: false,
      lineTension: 0
    }
  }


  drawChart(id: string, data, title: string){
    // console.log(id);

    var chartColors = {
      white: 'rgb(0,0,0)',
      green: 'rgb(75, 192, 192)',
      orange: 'rgb(255, 159, 64)',
      red: 'rgb(255, 99, 132)',
      yellow: 'rgb(255, 205, 86)',
      blue: 'rgb(54, 162, 235)',
      purple: 'rgb(153, 102, 255)',
      aqua: 'rgb(0, 255, 255)',
      navy: 'rgb(33, 111, 237)',
      lightgreen: 'rgb(32, 237, 32)',
      grey: 'rgb(201, 203, 207)'
    };

    var dataSets = [];
    var labels = Object.keys(data);
    var dates = data[Object.keys(data)[0]];
    for(var i = 0; i < dates.length; i++){
      dates[i] = dates[i].substr(6,2) + "/" + dates[i].substr(4,2) + "/" + dates[i].substr(0, 4);
    }
    var colorKeys = Object.keys(chartColors);
    if(title.includes("Ping") || title.includes("HTTP") || title.includes("Throughput")){
      for(var i = 1; i < Object.keys(data).length; i++){
        dataSets.push(this.dataSetLine(labels[i], data[labels[i]], chartColors[colorKeys[i]]));
      }
    }else{
      for(var i = 1; i < Object.keys(data).length; i++){
        dataSets.push(this.dataSetStacked(labels[i], data[labels[i]], chartColors[colorKeys[i]]));
      }
    }


    // console.log(dataSets);
    // console.log(labels);

    var ctx = (<HTMLCanvasElement>document.getElementById(id)).getContext('2d');

    var chartData = {
      labels: dates,
      datasets: dataSets

    };
    if(title.includes("Ping") || title.includes("HTTP") || title.includes("Throughput")){
      var lineChart = new Chart(ctx, {
        type: 'line',
        data: chartData,
        options: {
          title:{
            display: true,
            title: title
          },
          scales: {
            yAxes: [{
              scaleLabel: {
                display: true,
                labelString: title.replace(" All", ""),
                fontColor: 'rgb(31,65,93)',
                fontSize: 16
              }
            }],
            xAxes: [{
              scaleLabel: {
                display: true,
                labelString: 'Time',
                fontColor: 'rgb(31,65,93)',
                fontSize: 16
              }
            }]
          }
        },
      });
    }else{
      var stackedBar = new Chart(ctx, {
        type: 'bar',
        data: chartData,
        options: {
          title: {
            display: true,
            text: title
          },
          scales: {
            xAxes: [{
              stacked: true,
              fontColor: 'rgb(31,65,93)',
              fontSize: 16
            }],
            yAxes: [{
              stacked: true,
              ticks: {
                min: 0,
                max: 100,
                callback: function(value) {
                  return value + "%";
                },
                fontColor: 'rgb(31,65,93)',
                fontSize: 16
              }
            }]
          },
          // maintainAspectRatio: false,
          // legend: {
          //   position: 'bottom'
          // },
          plugins: {
            stacked100: { enable: true }
          }
        }
      });
    }








  }

  adjustHeight(){
    setTimeout(()=>{
      var windowHeight = $(window).height();
      var header = $('#headerLiveMonitoring');
      var tabList = $("#categoryList");
      var content = $("#categoryContent");
      var dashboard = $("#dashboard");

      content.height(windowHeight - (tabList.height() + header.height()));
      dashboard.height(windowHeight - (tabList.height() + header.height()));

      for(var i = 0; i < this.categories.length; i++){
        var subTabList = $("#" + this.categories[i] + "SubCategoryTabList");
        var subContent = $("#" + this.categories[i] + "SubCategoryContent");
        subContent.height(content.height() - subTabList.height());
      }

    }, 200);
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";


import { AppComponent } from './app.component';
import { LiveMonitoringComponent } from './live-monitoring/live-monitoring.component';
import { ModulesComponent } from './modules/modules.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from "./app-routing/app-routing.module";
import { WebsocketService } from "./websocket.service";
import { ActiveTestComponent } from './active-test/active-test.component';
import { MonitorComponent } from './monitor/monitor.component';
import {UserService} from "./user.service";
import { AboutComponent } from './about/about.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FooterComponent } from './footer/footer.component';
import { CustomerexperienceComponent } from './customerexperience/customerexperience.component';
import { ConfigurationBrowserComponent } from './configuration-browser/configuration-browser.component';


@NgModule({
  declarations: [
    AppComponent,
    LiveMonitoringComponent,
    ModulesComponent,
    LoginComponent,
    ActiveTestComponent,
    MonitorComponent,
    AboutComponent,
    HeaderComponent,
    DashboardComponent,
    FooterComponent,
    CustomerexperienceComponent,
    ConfigurationBrowserComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    WebsocketService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

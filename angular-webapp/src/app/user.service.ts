import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {WebsocketService} from "./websocket.service";

@Injectable()
export class UserService implements CanActivate {

  private username: string = "TestUser123";
  private loggedIn = false;
  private operator = "";
  private reload = false;
  private timeoutID;
  private timeoutLength = 600000;
  // private timeoutLength = 10000;
  private intervalID;
  private imsiList = [];
  // private routeURLs = ["login", "live-monitoring", "configuration-browser", "customer-experience"];

  constructor(private router: Router, private websocketService: WebsocketService) {

    if(sessionStorage.getItem("login") === "true"){
      this.loggedIn = true;
      this.setupTimer();
      this.intervalID = setInterval(()=>{
          try{
            var imsiQuery = {"initialize": [["basic", "IMSI"]]};
            this.websocketService.send(JSON.stringify(imsiQuery));
          }catch(e){
          }
        }, 1000);
    }
    if(sessionStorage.getItem("username") != ""){
      this.username = sessionStorage.getItem("username");
    }
    if(sessionStorage.getItem("operator") != ""){
      this.operator = sessionStorage.getItem("operator");
    }

    this.websocketService.change.subscribe(message => {
      if((typeof message) == "object"){
        console.log("HERE ");
        console.log(message);
        if(message.IMSI){
          this.imsiList = [];
          for(var i = 0; i < message.IMSI.length; i++){
            this.imsiList.push(message.IMSI[i]);
          }
          clearInterval(this.intervalID);
        }
      }
    });
  }

  setUsername(username : string){
    this.username = username;
    sessionStorage.setItem("username", username);
  }

  getUsername(): string{
    return this.username;
  }


  getLoggedIn(): boolean {
    return this.loggedIn;
  }

  setLoggedIn(value: boolean) {
    this.loggedIn = value;
    sessionStorage.setItem("login", "" + value);
    if(value){
      this.setupTimer();
      var imsiQuery = { "initialize": [["basic", "IMSI"]] };
      console.log(imsiQuery);
      this.websocketService.send(JSON.stringify(imsiQuery));
    }
  }

  getOperator(): string {
    return this.operator;
  }

  setOperator(value: string) {
    this.operator = value;
    sessionStorage.setItem("operator", value);
  }


  getReload(): boolean {
    return this.reload;
  }

  setReload(value: boolean) {
    this.reload = value;
  }

  verifyLogin(route: ActivatedRouteSnapshot){
    if(this.loggedIn){
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // console.log(this.routeURLs.indexOf(route.url[0].path));
    return this.verifyLogin(route);
  }

  getIMSIs(): Array<string> {

    // var availableTags = [
    //   "602022178866007",
    //   "602022203305548",
    //   "602022206302841",
    //   "602022206374170",
    //   "602022206374258",
    //   "602022206374829",
    //   "602022212314008",
    //   "602022221778235",
    //   "602022235085662",
    //
    //   "602030022146419",
    //   "602030116559594",
    //   "602030926369963",
    //   "602030941586094",
    //   "602030950384765",
    //   "602030975471960",
    //   "602030995303044",
    //
    //   "602040000101353",
    //   "602040000101358",
    //   "602040155011116",
    //   "602041000161635"
    // ];
    var availableTags = this.imsiList;
    console.log(this.operator);
    console.log(availableTags);

    for(var i = 0; i < availableTags.length; i++){
      if(!this.imsiList[i].includes(this.operator)){
        availableTags[i] = '';
      }
    }
    var result = [];
    for(var i = 0; i < availableTags.length; i++){
      if(availableTags[i].length > 0){
        result.push(availableTags[i]);
      }
    }
    return result;
  }

  startTimer() {
    let service = this;
    this.timeoutID = setTimeout(function(){
      service.goInactive();
    }, service.timeoutLength);
    // console.log("SET");
  }


  resetTimer(e) {
    clearTimeout(this.timeoutID);

    this.goActive();
    this.startTimer();

    // console.log("RESET");
  }

  goActive() {
    // do something
    // console.log("More stuff");
  }

  logOut(){
    this.setLoggedIn(false);
    this.setOperator(null);
    this.setUsername("TestUser123");
    this.setReload(true);
    this.router.navigate(['/login']);
  }

  goInactive() {
    // do something
    this.logOut();
    // console.log("Stuff");
  }

  setupTimer() {
    if(this.timeoutID){
      clearTimeout(this.timeoutID);
    }
    addEventListener("mousemove", (e)=> this.resetTimer(e), false);
    addEventListener("mousedown", (e)=> this.resetTimer(e), false);
    addEventListener("keypress", (e)=> this.resetTimer(e), false);
    addEventListener("DOMMouseScroll", (e)=> this.resetTimer(e), false);
    addEventListener("mousewheel",(e)=> this.resetTimer(e), false);
    addEventListener("touchmove", (e)=> this.resetTimer(e), false);
    addEventListener("MSPointerMove",(e)=> this.resetTimer(e), false);

    this.startTimer();
  }


}

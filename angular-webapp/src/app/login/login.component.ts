import {AfterViewInit, Component, OnInit} from '@angular/core';
import {CanActivate, Router} from "@angular/router";
import {UserService} from "../user.service";
import {WebsocketService} from "../websocket.service";

declare var $: any;
declare var grecaptcha: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {

  constructor(public _router: Router, private userService: UserService, private websocketService: WebsocketService) {}

  ngOnInit() {
    if(this.userService.getLoggedIn()){
      this._router.navigateByUrl('/modules');
    }
    if(this.userService.getReload()){
      this.userService.setReload(false);
      location.reload();
    }
    this.websocketService.change.subscribe(message => {
      if(message.success){
        this.userService.setOperator(message.success[0]);
        this.userService.setLoggedIn(true);
        this.userService.setUsername($('#username').val());
        this._router.navigateByUrl('/modules');
      }else if(message.reject){
        alert("Incorrect username or password.");
      }
      }
    );


  }

  ngAfterViewInit(){
    this.adjustLogo();
  }

  adjustLogo(){
    var logo = $('.img-fluid');
    var documentWidth = $(document).width()/2;
    // console.log(documentWidth);
    logo.width($(document).width() / 4);
    // console.log($(document).width() / 3);
    // console.log(logo.width());
    var left = documentWidth - (logo.width()/2);
    logo.offset({top: 0, left: left});
  }


  login(): void {
    var username = (<HTMLInputElement>document.getElementById("username")).value;
    var password = (<HTMLInputElement>document.getElementById("pwd")).value;

    var response = grecaptcha.getResponse();

    if(response.length == 0){
      alert("Please do the recaptcha challenge before logging in.");
      return;
    }


    var json= {"login": [ [username] , [password] , [navigator.userAgent] ]} ;

    this.websocketService.send(JSON.stringify(json));



  }



}

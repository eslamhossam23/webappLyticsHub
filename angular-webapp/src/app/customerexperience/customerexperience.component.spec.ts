import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerexperienceComponent } from './customerexperience.component';

describe('CustomerexperienceComponent', () => {
  let component: CustomerexperienceComponent;
  let fixture: ComponentFixture<CustomerexperienceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerexperienceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerexperienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

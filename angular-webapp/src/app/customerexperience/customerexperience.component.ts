import {AfterViewInit, Component, OnInit} from '@angular/core';
import {WebsocketService} from "../websocket.service";
import {UserService} from "../user.service";
import {Time} from "../utilities/time";
import {KpitoMr} from "../utilities/kpito-mr";
import {KmlHandler} from "../utilities/kml-handler";
import {ChartHandler} from "../utilities/chart-handler";

declare var $: any;
declare var google: any;
declare var Chart: any;


@Component({
  selector: 'app-customerexperience',
  templateUrl: './customerexperience.component.html',
  styleUrls: ['./customerexperience.component.css']
})
export class CustomerexperienceComponent implements OnInit, AfterViewInit {

  public categories = ['4', '3','2'];
  public categoriesHumanNames = ['LTE KPIs', 'UMTS KPIs','GSM KPIs'];
  public subCategories = [["rsrp", "rsrq", "sinr"], ["rscp", "ecno", "rssi"], ["rssi"]];
  public subCategoriesHumanNames = [["RSRP", "RSRQ", "SINR"], ["RSCP", "ECNO", "RSSI"], ["RSSI"]];
  public queryNumber = 0;
  public queryCounter = 0;

  constructor(private userService: UserService, private webSocketService: WebsocketService) { }

  ngOnInit() {
    this.webSocketService.change.subscribe(message => {
      // console.log(message);
      if(!window.location.href.includes("customer-experience")){
        return;
      }
      var keys = Object.keys(message);
      var mr;
      for(var i = 0; i < keys.length; i++){
        if(keys[i].includes("avg(")){
          mr = keys[i].replace("avg(", "").replace(")", "");
          break;
        }
      }
      if(message.IMSI){
        setTimeout(()=>{
          $( "#imsiCE" ).autocomplete({
            source: this.userService.getIMSIs()
          });
        },1000);
      }else if(message.latitude && message.longitude){
        var category = message.technology[0];
        var subcategory = KpitoMr.kpi(mr, parseInt(category));
        var subcategoryLower = subcategory.toLowerCase();
        // console.log(category);
        // console.log(subcategory);
        // console.log(subcategoryLower);
        if(!(subcategoryLower + category + "Map")){
          return;
        }
        KmlHandler.KML(message, 8 * 0.0001, subcategory, subcategoryLower + category + "Map", subcategoryLower + category + "MapContainer", subcategoryLower + category + "Capture", subcategoryLower + category + "Legend", parseInt(category));
        $("#" + subcategoryLower + category + "Legend").show();
        this.queryCounter++;
      }
      else if(message.Date_Time){
        var category = message.technology[0];
        var subcategory = KpitoMr.kpi(mr, parseInt(category));
        var subcategoryLower = subcategory.toLowerCase();
        if(!(subcategoryLower + category + "Chart")){
          return;
        }
        ChartHandler.lineChart(message, subcategory, parseInt(category), subcategoryLower + category + "Chart");
        var chart = $("#" + subcategoryLower + category + "Chart");
        chart.show();
        chart.css("margin-top", ($("#" + subcategoryLower + category + "ChartContainer").height() - chart.height())/2);
        this.queryCounter++;
      }
      if(this.queryCounter === this.queryNumber){
        var queryButton = $("#queryButtonCE");
        queryButton.html('Query');
        queryButton.prop('disabled', false);
        this.queryCounter = 0;
      }

    });

    for(var i = 0; i < this.subCategories.length; i++){
      for(var j = 0; j < this.subCategories[i].length; j++){
        this.queryNumber++;
      }
    }

  }

  ngAfterViewInit(){
    // (<any>window).googleMapsReady=this.onMapsReady.bind(this);
    // var script = document.createElement("script");
    // script.type = "text/javascript";
    // var head = document.getElementsByTagName("head")[0];
    // for(var i = 0; i < head.children.length; i++){
    //   var child = head.children[i];
    //   console.log(child);
    //   var src = child.getAttribute("src");
    //   if(src && src.includes("maps.googleapis.com")){
    //     console.log("MAP REMOVAL IMMINENT.");
    //     console.log(child);
    //     head.removeChild(child);
    //     break;
    //   }
    // }
    // script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDA9vW6Uw02UZUjoscCFa-7BBzjyN4UX7E&callback=googleMapsReady";
    //
    //
    // head.appendChild(script);

    this.onMapsReady();

    this.adjustContentHeight();
    $(window).on("resize", () => {
      this.adjustContentHeight();
    });
    $( "#imsiCE" ).autocomplete({
      source: this.userService.getIMSIs()
    });

    Time.initTime("dateFromCE", "dateToCE", null, null);
  }

  onMapsReady(){
    console.log("Maps ready!");
    for(var i = 0; i < this.categories.length; i++){
      for(var j = 0; j < this.subCategories[i].length; j++){
        var id = this.subCategories[i][j] + this.categories[i] + 'Map';
        var chartID = this.subCategories[i][j] + this.categories[i] + 'Chart';
        var uluru = {lat: 30, lng: 31};
        var map = new google.maps.Map(document.getElementById(id), {
          zoom: 15,
          center: uluru,
          keyboardShortcuts: false,
          streetViewControl: false,
          fullscreenControl: false,
          zoomControl: false,
          zoomControlOptions: false,
          mapTypeId: 'roadmap'
        });
        // var ctx = (<HTMLCanvasElement>document.getElementById(chartID)).getContext('2d');

        // var chartData = {
        //   labels: ["1", "1", "1"],
        //   datasets: [{
        //     label: "Stuff",
        //     backgroundColor: "red",
        //     borderColor: "red",
        //     data: ["15", "20", "25"],
        //     fill: false,
        //     lineTension: 0
        //   }]
        //
        // };
        // var lineChart = new Chart(ctx, {
        //   type: 'line',
        //   data: chartData,
        //   options: {
        //     title:{
        //       display: true,
        //       title: "Sample Title"
        //     },
        //     scales: {
        //       yAxes: [{
        //         scaleLabel: {
        //           display: true,
        //           labelString: "Sample Title",
        //           fontColor: 'rgb(31,65,93)',
        //           fontSize: 16
        //         }
        //       }],
        //       xAxes: [{
        //         scaleLabel: {
        //           display: true,
        //           labelString: 'Time',
        //           fontColor: 'rgb(31,65,93)',
        //           fontSize: 16
        //         }
        //       }]
        //     }
        //   },
        // });







      }
    }



  }

  sendQuery(){
    var imsi = "=" + $('#imsiCE').val();
    var dateTime = Time.dateFormat("dateFromCE", "dateToCE", null, null);
    var overviewQuery = {"CE": [[imsi, dateTime[0], dateTime[1]]]};
    console.log(overviewQuery);
    this.webSocketService.send(JSON.stringify(overviewQuery));
    var query;
    // var mr = ["MR1", "MR2", "MR3", "MR4"];
    // var technology = ["2", "3", "4"];
    var queryType = "basic";
    var timeSensitivity = "10";
    var locationSensitivity = "0.001";

    for(var i = 0; i < this.categories.length; i++){
      for(var j = 0; j < this.subCategoriesHumanNames[i].length; j++){
        var mr = KpitoMr.mr(this.subCategoriesHumanNames[i][j], parseInt(this.categories[i]));
        query = {"query": [[queryType], [mr, "Date_Time", "technology"], ["all", "timeavg-" + timeSensitivity, "raw"], ["IMSI", "technology", "Date_Time"], [imsi, "=" + this.categories[i], dateTime[0] + "-\u003e" + dateTime[1] ]]};
        this.webSocketService.send(JSON.stringify(query));
        console.log(query);
        query = {"query": [[queryType], [mr, "latitude", "longitude", "technology"], ["all", "locationavg-" + locationSensitivity, "locationavg-" + locationSensitivity, "raw"], ["IMSI", "technology", "Date_Time"], [imsi, "=" + this.categories[i], dateTime[0] + "-\u003e" + dateTime[1] ]]};
        this.webSocketService.send(JSON.stringify(query));
        console.log(query);
      }
    }

    var queryButton = $("#queryButtonCE");
    queryButton.html('<i class="fa fa-spinner fa-spin" style="color: white; font-size: 18px;"></i>');
    queryButton.prop('disabled', true);

    // query = {"query": [[queryType], [mr[0], "Date_Time"], ["all", "timeavg-" + timeSensitivity], ["IMSI", "technology", "Date_Time", "accuracy"], [imsi, techno, startDateString + "-\u003e" + endDateString, 30 + "-\u003e" + 10000 ]]};

    // TODO: 4G EARFCN, 3G UARFCN, 2G ARFCN, 4G PCI, 3G PSC, 2G BSIC
    // tac_lac, freq, lci

    // {"initialize":[[esm el table],[parameters group by]]}

    // TODO: frequency
    // {"initialize":[[basic],[freq]]}

    // TODO: take imsis from database

    // {"initialize":[[basic],[IMSI]]}


  }

  adjustContentHeight(){
    var height = $(window).height();
    var ceHeaderHeight = $('#ceHeader').height();
    var ceTabsHeight = $('#ceTabs').height();
    // var footerHeight = $('#footer').height();
    var queryCriteriaCE = $('#queryCriteriaCE');
    var queryCriteriaHeight = queryCriteriaCE.height() + parseFloat(queryCriteriaCE.css("margin-bottom"));
    // var contentHeight = height - (ceHeaderHeight + queryCriteriaHeight + ceTabsHeight + footerHeight);
    var contentHeight = height - (ceHeaderHeight + queryCriteriaHeight + ceTabsHeight);
    // console.log(height);
    // console.log(ceHeaderHeight);
    // console.log(ceTabsHeight);
    // console.log(contentHeight);
    // console.log(footerHeight);
    var ceTabContent = $('#ceTabContent');
    ceTabContent.height(contentHeight);
    $('.cePane').height(ceTabContent.height() - ceTabsHeight);
    // console.log(footerHeight);
    // console.log(ceHeaderHeight + ceTabsHeight + footerHeight + contentHeight);
    var queryButtonCE = $('#queryButtonCE');
    var dateFromCE = $('#dateFromCE');
    queryButtonCE.css("position", "absolute");
    queryButtonCE.css("top", dateFromCE.position().top + 'px');
    queryButtonCE.height(dateFromCE.height());
    // console.log($('#queryButtonCE').position().top);
    // console.log($('#dateFromCE').position().top);
  }

  adjustChartHeight(subcategory, category){
    setTimeout(function () {
      var chart = $("#" + subcategory + category + "Chart");
      // console.log(chart);
      // console.log(chart.css("margin-top"));
      chart.css("margin-top", ($("#" + subcategory + category + "ChartContainer").height() - chart.height())/2);
      // console.log(chart.css("margin-top"));
    }, 300);

  }

// Query list
// {query=[[basic], [MR1, Date_Time], [all, timeavg-10], [IMSI, technology, Date_Time], [=602040000101353, =2, 20180416000000->20180426235959]]}
// {query=[[basic], [MR1, Date_Time], [all, timeavg-10], [IMSI, technology, Date_Time], [=602040000101353, =3, 20180416000000->20180426235959]]}
// {query=[[basic], [MR3, Date_Time], [all, timeavg-10], [IMSI, technology, Date_Time], [=602040000101353, =3, 20180416000000->20180426235959]]}
// {query=[[basic], [MR4, Date_Time], [all, timeavg-10], [IMSI, technology, Date_Time], [=602040000101353, =3, 20180416000000->20180426235959]]}
// {query=[[basic], [MR1, Date_Time], [all, timeavg-10], [IMSI, technology, Date_Time], [=602040000101353, =4, 20180416000000->20180426235959]]}
// {query=[[basic], [MR2, Date_Time], [all, timeavg-10], [IMSI, technology, Date_Time], [=602040000101353, =4, 20180416000000->20180426235959]]}
// {query=[[basic], [MR3, Date_Time], [all, timeavg-10], [IMSI, technology, Date_Time], [=602040000101353, =4, 20180416000000->20180426235959]]}

// {query=[[basic], [MR1, latitude, longitude], [all, locationavg-0.001, locationavg-0.001], [IMSI, technology, Date_Time], [=602040000101353, =2, 20180416000000->20180426235959]]}
// {query=[[basic], [MR1, latitude, longitude], [all, locationavg-0.001, locationavg-0.001], [IMSI, technology, Date_Time], [=602040000101353, =3, 20180416000000->20180426235959]]}
// {query=[[basic], [MR3, latitude, longitude], [all, locationavg-0.001, locationavg-0.001], [IMSI, technology, Date_Time], [=602040000101353, =3, 20180416000000->20180426235959]]}
// {query=[[basic], [MR4, latitude, longitude], [all, locationavg-0.001, locationavg-0.001], [IMSI, technology, Date_Time], [=602040000101353, =3, 20180416000000->20180426235959]]}
// {query=[[basic], [MR1, latitude, longitude], [all, locationavg-0.001, locationavg-0.001], [IMSI, technology, Date_Time], [=602040000101353, =4, 20180416000000->20180426235959]]}
// {query=[[basic], [MR2, latitude, longitude], [all, locationavg-0.001, locationavg-0.001], [IMSI, technology, Date_Time], [=602040000101353, =4, 20180416000000->20180426235959]]}
// {query=[[basic], [MR3, latitude, longitude], [all, locationavg-0.001, locationavg-0.001], [IMSI, technology, Date_Time], [=602040000101353, =4, 20180416000000->20180426235959]]}




  collapse(subcategory: string, category: string, type: string){
    var mapContainer = $('#' + subcategory + category + 'MapContainer');
    var chartContainer = $('#' + subcategory + category + 'ChartContainer');
  }

}

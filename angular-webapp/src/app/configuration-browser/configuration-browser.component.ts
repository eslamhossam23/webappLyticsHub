import {AfterViewInit, Component, OnInit} from '@angular/core';
import {WebsocketService} from "../websocket.service";
import {saveAs} from "file-saver";

declare var $: any;
declare var XLSX: any;

@Component({
  selector: 'app-configuration-browser',
  templateUrl: './configuration-browser.component.html',
  styleUrls: ['./configuration-browser.component.css']
})
export class ConfigurationBrowserComponent implements OnInit, AfterViewInit {

  public vendors = [];
  public vendorSites = {};
  public vendorTableNames = [];
  public vendorTableParams = [];
  public responseTableNames = [];
  public responseTableParams = [];
  public responseTableValues = [];

  private intervalID;


  constructor(private websocketService: WebsocketService) { }

  ngOnInit() {


    this.intervalID = setInterval(()=> {
      try{
        this.initQuery();
      }catch (e){

      }

    }, 1000);

    this.websocketService.change.subscribe(message =>{
      if(!message.response){
        return;
      }
      if(message.response.initialize){
        clearInterval(this.intervalID);

        if(this.vendors.length > 0){
          return;
        }
        var keys = Object.keys(message);
        for(var i = 0; i < keys.length; i++){
          var vendor = keys[i];
          if(vendor === "response"){
            continue;
          }
          var vendorCapitalName = vendor.charAt(0).toUpperCase() + vendor.substr(1, vendor.length);
          this.vendors.push(vendorCapitalName);
          var vendorSites = message[vendor]['siteid'];
          this.vendorSites[vendorCapitalName] = [];
          for(var j = 0; j < vendorSites.length; j++){
            for(var x = 0; x < vendorSites[j].length; x++){
              this.vendorSites[vendorCapitalName].push(vendorSites[j][x]);
            }
          }
          var keys2 = Object.keys(message[vendor]);

          this.vendorTableNames.push([]);
          this.vendorTableParams.push([]);
          for(var j = 0; j < keys2.length; j++){
            var tableName = keys2[j];
            if(tableName === "siteid"){
              continue;
            }
            this.vendorTableNames[this.vendorTableNames.length - 1].push(tableName);
            this.vendorTableParams[this.vendorTableParams.length - 1].push([message[vendor][tableName][0]]);
          }
        }
        setTimeout(()=>{
          for(var i = 0; i < this.vendors.length; i++) {
            var multiSelect = $('#' + this.vendors[i] + "SingleSiteMultipleSelect");
            multiSelect.multipleSelect({
              placeholder: "Please select Site(s)...",
              position: 'top'
            });

            multiSelect.children("option").remove();

            multiSelect.multipleSelect('refresh');

            for(var j = 0; j < this.vendorSites[this.vendors[i]].length; j++){
              $('<option />', {value: this.vendorSites[this.vendors[i]][j], text: this.vendorSites[this.vendors[i]][j]}).appendTo("#" + this.vendors[i] + "SingleSiteMultipleSelect");
            }
            multiSelect.multipleSelect('refresh');


            // var uiAutoComplete = $(".ui-autocomplete");
            // uiAutoComplete.css("max-height", "300px");
            // uiAutoComplete.css("overflow-y", "auto");
            // uiAutoComplete.css("overflow-x", "hidden");
          }
        }, 1500);
        console.log(this.vendors);
        console.log(this.vendorTableNames);
        console.log(this.vendorTableParams);
      }else if(message.response.query){
        this.responseTableNames = [];
        this.responseTableParams = [];
        this.responseTableValues = [];
        for(var i = 0; i < this.vendors.length; i++){
          var vendorName = this.vendors[i].toLowerCase();
          if(message[vendorName]){
            var keys = Object.keys(message[vendorName]);
            console.log(keys);
            for(var j = 0; j < keys.length; j++){
              this.responseTableNames.push(keys[j]);
              this.responseTableParams.push([]);
              this.responseTableParams[this.responseTableParams.length - 1].push(message[vendorName][keys[j]][0]);
              this.responseTableValues.push([]);
              for(var x = 1; x < message[vendorName][keys[j]].length; x++){
                this.responseTableValues[this.responseTableValues.length - 1].push(message[vendorName][keys[j]][x]);
              }

            }
          }
          var queryButton = $("#" + this.vendors[i] + "QueryButton");
          queryButton.html('Query');
          queryButton.prop('disabled', false);
        }

        this.refreshMultipleSelect();


        setTimeout(()=> {
          this.adjustHeight();
        }, 100);
        console.log(this.responseTableNames);
        console.log(this.responseTableParams);
        console.log(this.responseTableValues);
      }




    });

  }



  ngAfterViewInit(){
    this.adjustHeight();
    $(window).on("resize", () => {
      this.adjustHeight();
    });
  }

  adjustHeight(){
    var windowHeight = $(window).height();
    var cbHeader = $('#cbHeader');
    // var cbVendors = $('#cbVendors');
    // var cbSideBar = $('#cbSideBar');
    // var cbVendorTables = $('#cbVendorTables');
    var cbContent = $('#cbContent');
    // var footer = $('#footer');
    var cbVendorTablesResult = $('#cbVendorTablesResult');
    var cbVendorTablesContent = $('#cbVendorTablesContent');
    // cbSideBar.height(windowHeight - cbHeader.height());
    // cbContent.height(windowHeight - cbHeader.height() - footer.height());
    cbContent.height(windowHeight - cbHeader.height());
    // cbVendorTables.height($('.cbSideBarUpperHalf').height() - cbHeader.height());
    cbVendorTablesContent.height(windowHeight - (cbHeader.height() + cbVendorTablesResult.height()));
  }


  initQuery(){
    var query = {"configuration": [["initialize"]]};


    this.websocketService.send(JSON.stringify(query));
  }


  sendQuery(){
    // var query = {"configuration": [["query"], [/*vendors*/], [/*vendor1 + tablename*/], [/*vendor2*/]};
    var vendors = [];
    var vendorTables = [];
    var vendorParams = [];
    var vendorSites = [];
    for(var i = 0; i < this.vendors.length; i++){
      var vendor = this.vendors[i];
      var checkedTables = $('input[name=' + vendor + ']:checked');
      if(checkedTables.length > 0){
        vendors.push(vendor.toLowerCase());
      }else{
        continue;
      }
      var siteType = $('input[name=' + vendor + 'Site]:checked').val();
      if(siteType === "all"){
        vendorSites.push(["all"]);
      }else if(siteType === "single"){
        vendorSites.push($('#' + vendor + "SingleSiteMultipleSelect").multipleSelect('getSelects'));
        console.log(vendorSites);
      }
      vendorTables.push([]);
      for(var j = 0; j < checkedTables.length; j++){
        var tableName = checkedTables[j].value;
        vendorTables[vendors.length - 1].push(tableName);
        var checkedParams = $('input[name=' + vendor + tableName + 'Param]:checked');
        vendorParams[vendors.length - 1] = [];
        for(var x = 0; x < checkedParams.length; x++){
          var param = checkedParams[x].value;
          vendorParams[vendors.length - 1].push(param);
        }
      }
      var queryButton = $("#" + vendor + "QueryButton");
      queryButton.html('<i class="fa fa-spinner fa-spin" style="color: #1F415D; font-size: 18px;"></i>');
      queryButton.prop('disabled', true);
    }
    console.log(vendors);
    console.log(vendorTables);
    // console.log(vendorParams);

    var query = {"configuration": [["query"]]};
    query["configuration"].push(vendors);


    for(var i = 0; i < vendors.length; i++){
      if(vendorTables[i].length > 0){
        query["configuration"].push(vendorTables[i]);
        // query["configuration"].push(vendorParams[i]);
      }
    }
    for(var i = 0; i < vendors.length; i++){
      if(vendorTables[i].length > 0){
        query["configuration"].push(vendorSites[i]);
      }
    }
    // query["configuration"].push(vendorSites);
    // query["configuration"].push(["all", "ISINS36999"]);
    // query["configuration"].push(["ISINS36999"]);
    // query["configuration"].push(["11111"]);

    // var query = {"configuration": [["query"], ["Ericsson"], ["admissioncontrol"], [""]]};

    console.log(query);
    this.websocketService.send(JSON.stringify(query));


  }


  search(event, vendor){
    // console.log(event);
    // console.log(event.target.value);
    // console.log(vendor);
    // console.log($('input[name=' + vendor + ']'));



    var tableNames = $('input[name=' + vendor + ']');

    var filter = event.target.value.toLowerCase();

    var searchType = $('input[name=' + vendor + 'Search]:checked').val();
    if(searchType === "table"){
      tableNames.each( function (index){

        var vendorTableName = $(this).val();

        var allParamGroup = $('#' + vendor + vendorTableName + "ParamGroup");

        // allParamGroup.show();

        var container = $('#' + vendor + vendorTableName + "Container");
        var containerButton = container.find("button");



        if(vendorTableName.toLowerCase().indexOf(filter) > -1){
          container.show();
          // var html = "";
          // for(var i = 0; i < vendorTableName.length; i++){
          //   if(filter.indexOf(vendorTableName.charAt(i).toLowerCase()) > -1){
          //     html += "<span style='background-color: orange'>" + vendorTableName.charAt(i) + "</span>";
          //   }else{
          //     html += vendorTableName.charAt(i);
          //   }
          // }
          // $(this).parent().children("label").children("span").html(html);
          if(!allParamGroup.is(":visible")){
            containerButton.html('<i class="fa fa-angle-down" style="font-size: 18px; color: #1F415D"></i>');
          }
        }else{
          container.hide();
        }
      });
    }else if(searchType === "param"){
      tableNames.each( function (index){

        var vendorTableName = $(this).val();

        var allParamGroup = $('#' + vendor + vendorTableName + "ParamGroup");

        allParamGroup.show();

        var container = $('#' + vendor + vendorTableName + "Container");
        var containerButton = container.find("button");


        var inputGroup = $("#" + vendor + vendorTableName + "Group");
        var found = false;

        var tableParams = $('input[name=' + vendor + vendorTableName + "Param]");
        tableParams.each(function (index) {
          var paramValue = $(this).val();
          var paramGroup = $('#' + vendor + vendorTableName + paramValue + "Group");
          var container = $('#' + vendor + vendorTableName + "Container");
          if(filter === ""){
            allParamGroup.hide();
            containerButton.html('<i class="fa fa-angle-down" style="font-size: 18px; color: #1F415D"></i>');
            return;
          }
          if(paramValue.toLowerCase().indexOf(filter) > -1){
            container.show();
            allParamGroup.show();
            paramGroup.show();
            found = true;
            // var html = "";
            // for(var i = 0; i < paramValue.length; i++){
            //   if(filter.indexOf(paramValue.charAt(i).toLowerCase()) > -1){
            //     html += "<span style='background-color: orange'>" + paramValue.charAt(i) + "</span>";
            //   }else{
            //     html += paramValue.charAt(i);
            //   }
            // }
            // $(this).parent().children("label").html(html);
            // console.log($(this).parent().parent().parent());
            containerButton.html('<i class="fa fa-angle-up" style="font-size: 18px; color: #1F415D"></i>');
            // console.log($(this).parent().parent().parent());

          }else{
            paramGroup.hide();
          }


        });

        if(!found && filter != ""){
          container.hide();
          return;
        }
        if(allParamGroup.is(":visible")){
          container.height(inputGroup.height() + allParamGroup.height());
        }else{
          container.height(37);
        }
      });
    }










    // tableNames.each( function (index){
    //
    //   var vendorTableName = $(this).val();
    //
    //   var allParamGroup = $('#' + vendor + vendorTableName + "ParamGroup");
    //
    //   allParamGroup.show();
    //
    //   var container = $('#' + vendor + vendorTableName + "Container");
    //
    //   if(vendorTableName.toLowerCase().indexOf(filter) > -1){
    //     container.show();
    //     // var html = "";
    //     // for(var i = 0; i < vendorTableName.length; i++){
    //     //   if(filter.indexOf(vendorTableName.charAt(i).toLowerCase()) > -1){
    //     //     html += "<span style='background-color: orange'>" + vendorTableName.charAt(i) + "</span>";
    //     //   }else{
    //     //     html += vendorTableName.charAt(i);
    //     //   }
    //     // }
    //     // $(this).parent().children("label").children("span").html(html);
    //     container.find("button").html('<i class="fa fa-angle-up" style="font-size: 18px; color: #1F415D"></i>');
    //   }else{
    //     container.hide();
    //     $(this).parent().hide();
    //     $(this).parent().children("label").children("span").html(vendorTableName);
    //   }
    //
    //
    //   var inputGroup = $("#" + vendor + vendorTableName + "Group");
    //
    //
    //   var tableParams = $('input[name=' + vendor + vendorTableName + "Param]");
    //   tableParams.each(function (index) {
    //     var paramValue = $(this).val();
    //     var paramGroup = $('#' + vendor + vendorTableName + paramValue + "Group");
    //     var container = $('#' + vendor + vendorTableName + "Container");
    //     if(paramValue.toLowerCase().indexOf(filter) > -1){
    //       container.show();
    //       inputGroup.show();
    //       allParamGroup.show();
    //       paramGroup.show();
    //       // var html = "";
    //       // for(var i = 0; i < paramValue.length; i++){
    //       //   if(filter.indexOf(paramValue.charAt(i).toLowerCase()) > -1){
    //       //     html += "<span style='background-color: orange'>" + paramValue.charAt(i) + "</span>";
    //       //   }else{
    //       //     html += paramValue.charAt(i);
    //       //   }
    //       // }
    //       // $(this).parent().children("label").html(html);
    //       // console.log($(this).parent().parent().parent());
    //       $(this).parent().parent().parent().find("button").html('<i class="fa fa-angle-up" style="font-size: 18px; color: #1F415D"></i>');
    //       // console.log($(this).parent().parent().parent());
    //
    //     }else{
    //       paramGroup.hide();
    //       $(this).parent().children("label").html(paramValue);
    //     }
    //
    //     if(filter === ""){
    //       allParamGroup.hide();
    //       container.find("button").html('<i class="fa fa-angle-down" style="font-size: 18px; color: #1F415D"></i>');
    //     }
    //   });
    //   if(allParamGroup.is(":visible")){
    //     container.height(inputGroup.height() + allParamGroup.height());
    //   }else{
    //     container.height(37);
    //   }
    // });


  }

  collapseParams(vendor, vendorTableName){
    var checkbox = $("#" + vendor + vendorTableName)[0];
    var checked = checkbox.checked;
    var checkedChildren = [];
    var paramCheckboxes = $('input[name=' + vendor + vendorTableName + "Param]");

    paramCheckboxes.each(function (index) {
      checkedChildren[index] = $(this)[0].checked;
    });


    event.stopPropagation();
    var inputGroup = $("#" + vendor + vendorTableName + "Group");
    var paramGroup = $("#" + vendor + vendorTableName + "ParamGroup");
    var html = inputGroup.find("button").html();
    if(html.includes("fa-angle-down")){
      paramGroup.show();
      $('#' + vendor + vendorTableName + "Container").height(inputGroup.height() + paramGroup.height());
      inputGroup.find("button").html('<i class="fa fa-angle-up" style="font-size: 18px; color: #1F415D"></i>');
    }else{
      paramGroup.hide();
      $('#' + vendor + vendorTableName + "Container").height(37);
      inputGroup.find("button").html('<i class="fa fa-angle-down" style="font-size: 18px; color: #1F415D"></i>');
    }

    setTimeout(function () {
      checkbox.checked = checked;
      paramCheckboxes.each(function (index) {
        $(this)[0].checked = checkedChildren[index];
      });
    }, 1);

  }

  checkParent(vendor, vendorTableName){
    var checkedInputs = $('input[name=' + vendor + vendorTableName + "Param]:checked");
    var parentInput = $('#' + vendor + vendorTableName + "Group");
    if(checkedInputs.length > 0){
      parentInput.children("input")[0].checked = true;
    }else{
      parentInput.children("input")[0].checked = false;
    }
  }

  checkChildren(vendor, vendorTableName){
    var checked = $('#' + vendor + vendorTableName + "Group").children("input")[0].checked;
    $('input[name=' + vendor + vendorTableName + "Param]").each(function (index) {
      $(this)[0].checked = checked;
    });
  }

  close(index){
    this.responseTableNames.splice(index, 1);
    this.responseTableParams.splice(index, 1);
    this.responseTableValues.splice(index, 1);
    if(this.responseTableNames.length === 0){
      for(var i = 0; i < this.vendors.length; i++) {
        var multiSelectContainer = $('#' + this.vendors[i] + 'TableMultipleSelectContainer');
        multiSelectContainer.hide();
      }
    }else{
      this.refreshMultipleSelect();
    }
  }


  exportExcel(index){

    for(var i = 0; i < this.vendors.length; i++){
      var tablesToBePrinted = $('#' + this.vendors[i] + 'TableMultipleSelect').multipleSelect('getSelects');
      console.log(tablesToBePrinted);

      for(var j = 0; j < tablesToBePrinted.length; j++){
        var tableName = tablesToBePrinted[j];
        console.log(tableName);
        var params = this.responseTableParams[index][0];
        console.log(params);
        var table = document.getElementById(tableName + "Table");
        var workbook = XLSX.utils.table_to_book(table);

        var wscols = [];


        for(var x = 0; x < params.length; x++){
          wscols.push({wch:20});
        }


        console.log(workbook);
        workbook.Sheets['Sheet1']['!cols'] = wscols;
        console.log(workbook.Sheets['Sheet1']);
        var wopts = { bookType:'xlsx', bookSST:false, type:'binary' };
        var wbout = XLSX.write(workbook,wopts);
        var fct = function s2ab(s) {
          var buf = new ArrayBuffer(s.length);
          var view = new Uint8Array(buf);
          for (var z=0; z!=s.length; ++z) view[z] = s.charCodeAt(z) & 0xFF;
          return buf;
        };
        /* the saveAs call downloads a file on the local machine */
        saveAs(new Blob([fct(wbout)],{type:"application/octet-stream"}), tableName +".xlsx");

      }
    }


  }

  refreshMultipleSelect(){
    for(var i = 0; i < this.vendors.length; i++){
      var multiSelectContainer = $('#' + this.vendors[i] + 'TableMultipleSelectContainer');
      if(this.responseTableNames.length === 0){
        multiSelectContainer.hide();
        continue;
      }
      var multiSelect = $('#'+ this.vendors[i] + 'TableMultipleSelect');
      multiSelect.multipleSelect(
        {
          placeholder: "Please select the tables to be exported...",
          position: 'top'
        }
      );
      multiSelect.children("option").remove();

      multiSelect.multipleSelect('refresh');

      for(var j = 0; j < this.responseTableNames.length; j++){
        $('<option />', {value: this.responseTableNames[j], text: this.responseTableNames[j]}).appendTo("#" + this.vendors[i] + "TableMultipleSelect");
      }
      multiSelect.multipleSelect('refresh');
      multiSelectContainer.show();
    }
  }

  collapseAll(vendor, table){

    if(table){
      var tableNames = $('input[name=' + vendor + ']');

      tableNames.each(function (index) {

        var vendorTableName = $(this).val();

        var allParamGroup = $('#' + vendor + vendorTableName + "ParamGroup");

        allParamGroup.hide();

        var container = $('#' + vendor + vendorTableName + "Container");
        var containerButton = container.find("button");

        containerButton.html('<i class="fa fa-angle-down" style="font-size: 18px; color: #1F415D"></i>');

        container.height(37);

      });
    }

    var searchValue = $('#' + vendor + 'Search').val();
    this.search({target: {value: searchValue}}, vendor);

  }


  toggleMultiSelectContainer(vendor, value){
    var container = $("#" + vendor + "SingleSiteContainer");
    if(value){
      container.show();
    }else{
      container.hide();
    }
  }




}

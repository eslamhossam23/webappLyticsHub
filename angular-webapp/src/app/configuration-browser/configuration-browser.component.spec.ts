import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationBrowserComponent } from './configuration-browser.component';

describe('ConfigurationBrowserComponent', () => {
  let component: ConfigurationBrowserComponent;
  let fixture: ComponentFixture<ConfigurationBrowserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurationBrowserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

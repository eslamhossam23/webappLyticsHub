export class Time {

  static initTime(startDate: string, endDate: string, startTime: string, endTime: string){
    var startDateInput = (<HTMLInputElement>document.getElementById(startDate));
    var startTimeInput = (<HTMLInputElement>document.getElementById(startTime));

    var endDateInput = (<HTMLInputElement>document.getElementById(endDate));
    var endTimeInput = (<HTMLInputElement>document.getElementById(endTime));

    var date = new Date();
    var month = "" + (date.getMonth() + 1);
    var day = "" + (date.getDate());
    var currDay = date.getDate();
    var startDay;
    if(Number(month) < 10){
      month = "0" + month;
    }
    if(currDay < 10){
      day = "0" + day;
      startDay = "01";
    }else if(currDay == 10){
      startDay = "01";
    }else if(currDay > 10 && currDay < 20){
      startDay = "0" + (currDay - 10);
    }else {
      startDay = currDay - 10;
    }
    if(startDateInput){
      startDateInput.value = date.getFullYear() + "-" + month + "-" + startDay;
    }
    if(endDateInput){
      endDateInput.value = date.getFullYear() + "-" + month + "-" + day;
    }

    if(startTimeInput){
      startTimeInput.value = Time.currentTime();
    }
    if(endTimeInput){
      endTimeInput.value = Time.currentTime();
    }
  }

  static currentTime(): string{

    var date = new Date();
    var hours = "" + date.getHours();
    var minutes = "" + date.getMinutes();
    var seconds = "" + date.getSeconds();
    if(Number(hours) < 10){
      hours = "0" + hours;
    }
    if(Number(minutes) < 10){
      minutes = "0" + minutes;
    }
    if(Number(seconds) < 10){
      seconds = "0" + seconds;
    }

    return hours + ":" + minutes + ":" + seconds;
  }

  static dateFormat(startDate: string, endDate: string, startTime: string, endTime: string): Array<string> {
    var startDateInput = (<HTMLInputElement>document.getElementById(startDate));
    var startDateVal = startDateInput.value.split('-');
    var startDateString;
    var startTimeInput = (<HTMLInputElement>document.getElementById(startTime));
    var startTimeVal;
    if(startTimeInput){
      startTimeVal = startTimeInput.value.split(':');
    }else{
      startTimeVal = Time.currentTime().split(":");
    }
    startDateString = startDateVal[0] + startDateVal[1] + startDateVal[2];
    if(startTimeVal.length > 2){
      startDateString += startTimeVal[0] + startTimeVal[1] + startTimeVal[2];
    }else{
      startDateString += startTimeVal[0] + startTimeVal[1] + "00";
    }

    var endDateInput = (<HTMLInputElement>document.getElementById(endDate));
    var endDateVal = endDateInput.value.split('-');
    var endDateString;
    var endTimeInput = (<HTMLInputElement>document.getElementById(endTime));
    var endTimeVal;
    if(endTimeInput){
      endTimeVal = endTimeInput.value.split(':');
    }else{
      endTimeVal = Time.currentTime().split(":");
    }
    endDateString = endDateVal[0] + endDateVal[1] + endDateVal[2];
    if(endTimeVal.length > 2){
      endDateString += endTimeVal[0] + endTimeVal[1] + endTimeVal[2];
    }else{
      endDateString += endTimeVal[0] + endTimeVal[1] + "00";
    }
    return [startDateString, endDateString];
  }

}

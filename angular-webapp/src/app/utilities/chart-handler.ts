import {KpitoMr} from "./kpito-mr";

declare var Chart: any;

export class ChartHandler {

  static lineCharts = {};

  static lineChart(message, kpiName, technology, chartID) {
    var dates = message.Date_Time;
    var mr = KpitoMr.mr(kpiName, technology);
    var key = "avg(" + mr + ")";
    // console.log("Key is: " + key);
    if (chartID.includes("undefined") || chartID.includes("mr")) {
      return;
    }
    var kpiValues = message[key];
    var points = dates.length;
    var rows = [];
    var labels = [];
    for (var i = 0; i < points; i++) {
      var date = dates[i];
      date.replace(/\//g, "");
      // console.log("Converted date: " + date);
      var day = date.substr(6, 2);
      var month = date.substr(4, 2);
      var year = date.substr(0, 4);
      var fullDate = day + "/" + month + "/" + year;
      rows.push(Number(kpiValues[i]));
      labels.push(fullDate);
      // rows.push([date, Number(kpiValues[i])]);
    }
    // console.log(rows);
    // console.log(labels);
    var ctx = (<HTMLCanvasElement>document.getElementById(chartID)).getContext('2d');
    if (ChartHandler.lineCharts[chartID]) {
      var oldChart = ChartHandler.lineCharts[chartID];
      oldChart.destroy();
    }
    ChartHandler.lineCharts[chartID] = new Chart(ctx, {
      type: 'line',
      data: {
        labels: labels,
        datasets: [{
          label: [kpiName],
          backgroundColor: 'rgb(31,65,93)',
          borderColor: 'rgb(31,65,93)',
          data: rows,
          fill: false,
          lineTension: 0,
        }],
      },
      options: {
        scales: {
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'KPI',
              fontColor: 'rgb(31,65,93)',
              fontSize: 16
            }
          }],
          xAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Time',
              fontColor: 'rgb(31,65,93)',
              fontSize: 16
            }
          }]
        }
      },
    });
  }

}

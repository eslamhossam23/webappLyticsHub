import {KpitoMr} from "./kpito-mr";

declare var google: any;
declare var geoXML3: any;
declare var $: any;

export class KmlHandler {

  constructor(){

  }

  static KML(results, resolution, kpiName, mapID, mapContainerID, captureID, legendID, technology){
    if(mapID.includes("undefined") || mapID.includes("mr")){
      return;
    }

    var map = new google.maps.Map(document.getElementById(mapID), {
      center: new google.maps.LatLng(30, 31),
      zoom: 15,
      keyboardShortcuts: false,
      streetViewControl: false,
      fullscreenControl: false,
      zoomControl: false,
      zoomControlOptions: false,
      mapTypeId: 'roadmap',
    });

    var geoXml = new geoXML3.parser({
      map: map,
      singleInfoWindow: true
    });

    var kmlString = this.createKMLHeader() + this.KMLpreparePolygons(results, resolution, kpiName, technology) + this.createKMLFooter();

    geoXml.parseKmlString(kmlString);

    map.setZoom(6);
    map.setCenter(new google.maps.LatLng(30, 31));
    //capture div gets consumed when using KML, so we need to recreate it
    var container = document.getElementById(mapContainerID);
    var firstChild = container.childNodes[1];
    if (container && firstChild && !document.getElementById(captureID)) {
      var capture = document.createElement("div");
      capture.id = captureID;
      firstChild.parentNode.insertBefore(capture, firstChild.nextSibling);
    }

    var ranges = this.ranges(kpiName);
    var range1 = ranges[1], range2 = ranges[2], range3 = ranges[3], range4 = ranges[4], range5 = ranges[5];
    var beginning = "max";
    var end = "min";
    var sign = " < ";
    if(range1 < 0){
      sign = " > ";
      beginning = "min";
      end = "max";
      range1 = ranges[0];
      range2 = ranges[1];
      range3 = ranges[2];
      range4 = ranges[3];
      range5 = ranges[4];
    }

    var legend = document.getElementById(legendID);
    if(legend === null){
      var legendTemp = document.createElement("div");
      legendTemp.id = legendID;
      legendTemp.style.fontFamily = "Arial, sans-serif";
      legendTemp.style.backgroundColor = "rgba(255,255,255,0.5)";
      legendTemp.style.padding = "10px";
      legendTemp.style.margin = "10px";
      legendTemp.style.border = "3px solid #000";
      container.appendChild(legendTemp);
      legend = document.getElementById(legendID);
    }
    while (legend.firstChild) {
      legend.removeChild(legend.firstChild);
    }

    var colors = this.colors(kpiName);
    console.log(colors);
    legend.innerHTML = "<h5>Legend</h5>";


    var name =  range1 + sign + kpiName + sign + beginning;
    var div = document.createElement('div');
    div.innerHTML = "<img src='../../assets/img/colorBackground.png' style='background-color: " + this.styleToColor(colors[0])  + "; margin-top: 2%'> " + name;
    legend.appendChild(div);



    var name = range2 + sign + kpiName + sign + range1;
    var div = document.createElement('div');
    div.innerHTML = "<img src='../../assets/img/colorBackground.png' style='background-color: " + this.styleToColor(colors[1])  + "; margin-top: 2%'> " + name;
    legend.appendChild(div);

    var name = range3 + sign + kpiName + sign + range2;
    var div = document.createElement('div');
    div.innerHTML = "<img src='../../assets/img/colorBackground.png' style='background-color: " + this.styleToColor(colors[2])  + "; margin-top: 2%'> " + name;
    legend.appendChild(div);

    var name = range4 + sign + kpiName + sign + range3;
    var div = document.createElement('div');
    div.innerHTML = "<img src='../../assets/img/colorBackground.png' style='background-color: " + this.styleToColor(colors[3])  + "; margin-top: 2%'> " + name;
    legend.appendChild(div);


    var name = range5 + sign + kpiName +  sign + range4;
    var div = document.createElement('div');
    div.innerHTML = "<img src='../../assets/img/colorBackground.png' style='background-color: " + this.styleToColor(colors[4])  + "; margin-top: 2%'> " + name;
    legend.appendChild(div);

    var name = end + sign + kpiName +  sign + range5;
    var div = document.createElement('div');
    div.innerHTML = "<img src='../../assets/img/colorBackground.png' style='background-color: " + this.styleToColor(colors[5])  + "; margin-top: 2%'> " + name;
    legend.appendChild(div);

    map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(legend);

  }

  static createKMLHeader(): string{
    return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\" xmlns:kml=\"http://www.opengis.net/kml/2.2\" xmlns:atom=\"http://www.w3.org/2005/Atom\">\n" +
      "   <Document>\n" +
      "   <name>LyticsHub</name>\n" +
      "   <open>1</open>\n"+
      "   <Style id=\"polygonBlack\">\n" +
      "       <LineStyle>\n" +
      "           <width>1.5</width>\n" +
      "           <color>ff000000</color>\n"+
      "       </LineStyle>\n" +
      "       <PolyStyle>\n" +
      "           <color>ff000000</color>\n" +
      "       </PolyStyle>\n" +
      "   </Style>\n"+
      "   <Style id=\"polygonRed\">\n" +
      "       <LineStyle>\n" +
      "           <width>1.5</width>\n" +
      "           <color>ff0000ff</color>\n"+
      "       </LineStyle>\n" +
      "       <PolyStyle>\n" +
      "           <color>ff0000ff</color>\n" +
      "       </PolyStyle>\n" +
      "   </Style>\n" +
      "   <Style id=\"polygonLightOrange\">\n" +
      "       <LineStyle>\n" +
      "           <width>1.5</width>\n" +
      "           <color>ff007fff</color>\n"+
      "       </LineStyle>\n" +
      "       <PolyStyle>\n" +
      "           <color>ff007fff</color>\n" +
      "       </PolyStyle>\n" +
      "   </Style>\n"  +
      "   <Style id=\"polygonYellow\">\n" +
      "       <LineStyle>\n" +
      "           <width>1.5</width>\n" +
      "           <color>ff00fffe</color>\n"+
      "       </LineStyle>\n" +
      "       <PolyStyle>\n" +
      "           <color>ff00fffe</color>\n" +
      "       </PolyStyle>\n" +
      "   </Style>\n" +
      "   <Style id=\"polygonLightGreen\">\n" +
      "       <LineStyle>\n" +
      "           <width>1.5</width>\n" +
      "           <color>ff00ff7f</color>\n"+
      "       </LineStyle>\n" +
      "       <PolyStyle>\n" +
      "           <color>ff00ff7f</color>\n" +
      "       </PolyStyle>\n" +
      "   </Style>\n"  +
      "   <Style id=\"polygonGreen\">\n" +
      "       <LineStyle>\n" +
      "           <width>1.5</width>\n" +
      "           <color>ff00ff00</color>\n"+
      "       </LineStyle>\n" +
      "       <PolyStyle>\n" +
      "           <color>ff00ff00</color>\n" +
      "       </PolyStyle>\n" +
      "   </Style>\n"  +
      "   <Style id=\"polygonGray\">\n" +
      "       <LineStyle>\n" +
      "           <width>1.5</width>\n" +
      "           <color>ff808080</color>\n"+
      "       </LineStyle>\n" +
      "       <PolyStyle>\n" +
      "           <color>ff808080</color>\n" +
      "       </PolyStyle>\n" +
      "   </Style>\n"   +
      "   <Style id=\"polygonOrange\">\n" +
      "       <LineStyle>\n" +
      "           <width>1.5</width>\n" +
      "           <color>ff0055ff</color>\n"+
      "       </LineStyle>\n" +
      "       <PolyStyle>\n" +
      "           <color>ff0055ff</color>\n" +
      "       </PolyStyle>\n" +
      "   </Style>\n"     +
      "   <Style id=\"polygonYellowOrange\">\n" +
      "       <LineStyle>\n" +
      "           <width>1.5</width>\n" +
      "           <color>ff00aaff</color>\n"+
      "       </LineStyle>\n" +
      "       <PolyStyle>\n" +
      "           <color>ff00aaff</color>\n" +
      "       </PolyStyle>\n" +
      "   </Style>\n"     +
      "   <Style id=\"polygonGreen\">\n" +
      "       <LineStyle>\n" +
      "           <width>1.5</width>\n" +
      "           <color>ff008000</color>\n"+
      "       </LineStyle>\n" +
      "       <PolyStyle>\n" +
      "           <color>ff008000</color>\n" +
      "       </PolyStyle>\n" +
      "   </Style>\n"     +
      "   <Style id=\"polygonLimeGreen\">\n" +
      "       <LineStyle>\n" +
      "           <width>1.5</width>\n" +
      "           <color>ff00ffa9</color>\n"+
      "       </LineStyle>\n" +
      "       <PolyStyle>\n" +
      "           <color>ff00ffa9</color>\n" +
      "       </PolyStyle>\n" +
      "   </Style>\n"   +
      "   <Style id=\"polygonLawnGreen\">\n" +
      "       <LineStyle>\n" +
      "           <width>1.5</width>\n" +
      "           <color>ff00ff54</color>\n"+
      "       </LineStyle>\n" +
      "       <PolyStyle>\n" +
      "           <color>ff00ff54</color>\n" +
      "       </PolyStyle>\n" +
      "   </Style>\n";
  }


  static createKMLFooter() : string{
    return "</Document>\n" + "</kml>";
  }

  // results is the received query result, resolution is sensitivity * 0.0001, kpiName is the selection after the technology
  static KMLpreparePolygons(results, resolution, kpiName, technology): string {
    var longitudeArray = results.longitude;
    var latitudeArray = results.latitude;
    // access first property of results which is avg(MR1)
    // var kpi = $('#kpi').val();
    var mr = KpitoMr.mr(kpiName, technology);
    // console.log("MR " + mr);
    var key = "avg(" + mr + ")";
    // console.log(key);
    var kpiValue = results[key];
    // console.log(kpiValue);
    // check user selected value of kpiName, possible values are: RSRP, RSCP, RSSI, SINR, RSRQ and ECNO
    var sign = false;
    // console.log(kpiName);
    var ranges = this.ranges(kpiName);
    var range0 = ranges[0], range1 = ranges[1], range2 = ranges[2], range3 = ranges[3], range4 = ranges[4], range5 = ranges[5];
    if(range1 < 0){
      sign = true;
    }
    // console.log(ranges);
    var polygon = "";

    var i;
    var style;
    var colors = this.colors(kpiName);
    // console.log("Length ");
    // console.log(longitudeArray);
    // console.log(kpiValue);
    for (i = 0; i < kpiValue.length; i++) {
      var value;
      try {
        if(kpiValue[i] === null){
          continue;
        }
        value = parseFloat(kpiValue[i]);
        if(sign){
          // var defaultColors = ["polygonBlack", "polygonRed", "polygonLightOrange", "polygonYellow", "polygonLightGreen", "polygonLawnGreen", "polygonGreen"];
          // var specialColors = ["polygonBlack", "polygonRed", "polygonOrange", "polygonYellowOrange", "polygonYellow", "polygonLimeGreen", "polygonLawnGreen", "polygonGreen"];

          // results[0] = -105;
          // results[1] = -90;
          // results[2] = -80;
          // results[3] = -70;
          // results[4] = -60;
          // results[5] = -40;

          // results[0] = -18;
          // results[1] = -15;
          // results[2] = -13;
          // results[3] = -11;
          // results[4] = -9;
          // results[5] = -7;
          if (value <= range0) {
            style = colors[0];
          } else if (value > range0 && value <= range1) {
            style = colors[1];
          } else if (value > range1 && value <= range2) {
            style = colors[2];
          }
          else if (value > range2 && value <= range3) {
            style = colors[3];
          } else if (value > range3 && value <= range4) {
            style = colors[4];
          } else if (value > range4 && value <= range5) {
            style = colors[5];
          } else if (value >= range5) {
            style = colors[6];
          } else if (value >= 0) {
            style = "polygonGray";
          } else {
            style = "polygonGray";
          }
        }else{
          // var defaultColors = ["polygonBlack", "polygonRed", "polygonLightOrange", "polygonYellow", "polygonLightGreen", "polygonLawnGreen", "polygonGreen"];
          // var smallColors = ["polygonBlack", "polygonRed", "polygonLightOrange", "polygonYellow", "polygonLightGreen"];
          // var specialColors = ["polygonBlack", "polygonRed", "polygonOrange", "polygonYellowOrange", "polygonYellow", "polygonLimeGreen", "polygonLawnGreen", "polygonGreen"];
          // results[0] = 30;
          // results[1] = 20;
          // results[2] = 15;
          // results[3] = 10;
          // results[4] = 5;
          // results[5] = 0;

          // results[0] = 20;
          // results[1] = 15;
          // results[2] = 10;
          // results[3] = 5;
          // results[4] = 3;
          // results[5] = 0;

          // results[0] = 250;
          // results[1] = 200;
          // results[2] = 150;
          // results[3] = 100;
          // results[4] = 50;
          // results[5] = 0;

          // results[0] = 25;
          // results[1] = 20;
          // results[2] = 15;
          // results[3] = 10;
          // results[4] = 5;
          // results[5] = 0;


          if (value >= range1) {
            style = colors[0];
          } else if (value < range1 && value >= range2) {
            style = colors[1];
          } else if (value < range2 && value >= range3) {
            style = colors[2];
          }
          else if (value < range3 && value >= range4) {
            style = colors[3];
          } else if (value < range4) {
            style = colors[4];
          }
        }
      } catch (e){
        value = 0;
        kpiValue[i] = 0;
      }

      var longitude = parseFloat(longitudeArray[i]);
      var leftEdge = longitude;
      var rightEdge = longitude + parseFloat(resolution);
      var latitude = parseFloat(latitudeArray[i]);
      var upperEdge = latitude + parseFloat(resolution);
      var lowerEdge = latitude;
      var point1 =  leftEdge+ "," + upperEdge + ",0";
      var point2 =  leftEdge + "," + lowerEdge + ",0";
      var point3 =  rightEdge + "," + lowerEdge + ",0";
      var point4 =  rightEdge + "," +  upperEdge + ",0";
      kpiName = $("#kpi").find(':selected').text();
      polygon += this.KMLdrawPolygon(point1, point2, point3, point4, style, kpiName, kpiValue[i]);
    }
    return polygon;
  }

  static KMLdrawPolygon(point1, point2, point3, point4, style, kpiName, kpiValue): string{
    return "   <Placemark>\n" +
      "       <name>"+ kpiName + "="+ kpiValue+"</name>\n" +
      "           <open>1</open>\n" +
      "           <styleUrl>#"+style+"</styleUrl>\n" +
      "           <Polygon>\n" +
      "               <tessellate>1</tessellate>\n" +
      "               <outerBoundaryIs>\n" +
      "                   <LinearRing>\n" +
      "                       <coordinates>\n" +
      "                          "+point1+" "+point2+" "+point3+" "+point4+" \n" +
      "                       </coordinates>\n" +
      "                   </LinearRing>\n" +
      "        	</outerBoundaryIs>\n" +
      "        </Polygon>\n" +
      "   </Placemark>\n";
  }

  static ranges(kpiName): Array<number> {
    var results = [];
    if (kpiName === "RSRP" || kpiName === "RSCP" || kpiName === "RSSI") {
      results[0] = -105;
      results[1] = -90;
      results[2] = -80;
      results[3] = -70;
      results[4] = -60;
      results[5] = -40;
    }
    else if (kpiName === "SINR") {
      results[0] = 25;
      results[1] = 20;
      results[2] = 15;
      results[3] = 10;
      results[4] = 5;
      results[5] = 0;
    }
    else if (kpiName === "RSRQ" || kpiName === "ECNO") {
      results[0] = -18;
      results[1] = -15;
      results[2] = -13;
      results[3] = -11;
      results[4] = -9;
      results[5] = -7;
    } else if(kpiName.includes("_P") || kpiName === "Ping" || kpiName.includes("_H") || kpiName === "HTTP_Delay"){
      results[0] = 250;
      results[1] = 200;
      results[2] = 150;
      results[3] = 100;
      results[4] = 50;
      results[5] = 0;
    }else if(kpiName.includes("DL")){
      results[0] = 30;
      results[1] = 20;
      results[2] = 15;
      results[3] = 10;
      results[4] = 5;
      results[5] = 0;
    }else if(kpiName.includes("UL")){
      results[0] = 20;
      results[1] = 15;
      results[2] = 10;
      results[3] = 5;
      results[4] = 3;
      results[5] = 0;
    }

    return results;
  }

  static colors(kpiName): Array<string>{
    var defaultColors = ["polygonBlack", "polygonRed", "polygonLightOrange", "polygonYellow", "polygonLightGreen", "polygonLawnGreen", "polygonGreen"];
    var smallColors = ["polygonBlack", "polygonRed", "polygonLightOrange", "polygonYellow", "polygonLightGreen"];
    var specialColors = ["polygonBlack", "polygonRed", "polygonOrange", "polygonYellowOrange", "polygonYellow", "polygonLimeGreen", "polygonLawnGreen", "polygonGreen"];
    switch (kpiName){
      case "RSRQ":
        return specialColors;
      case "ECNO":
        return specialColors;
      case "SINR":
        return defaultColors.reverse();
      case "DL_Internet":
        return defaultColors.reverse();
      case "DL":
        return defaultColors.reverse();
      case "UL":
        return defaultColors.reverse();
      case "Ping":
        return smallColors;
      case "Google_P":
        return smallColors;
      case "Facebook_P":
        return smallColors;
      case "YouTube_P":
        return smallColors;
      case "HTTP_Delay":
        return smallColors;
      case "Google_H":
        return smallColors;
      case "Facebook_H":
        return smallColors;
      case "YouTube_H":
        return smallColors;
      default:
        return defaultColors;
    }
  }

  static styleToColor(color): string{
    switch (color){
      case "polygonBlack":
        return "black";
      case "polygonRed":
        return "red";
      case "polygonOrange":
        return "#ff5500";
      case "polygonLightOrange":
        return "#ff7f00";
      case "polygonYellowOrange":
        return "#ffaa00";
      case "polygonYellow":
        return "yellow";
      case "polygonGreen":
        return "green";
      case "polygonLimeGreen":
        return "#a9ff00";
      case "polygonLightGreen":
        return "#7fff00";
      case "polygonLawnGreen":
        return "#54ff00";
      case "polygonGray":
        return "gray";
      default:
        return "gray";
    }
  }


}

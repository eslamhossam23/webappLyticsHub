export class KpitoMr {


  static mr(kpi: string, technology: number): string{
    switch (technology){
      case 2:
        switch (kpi){
          case "RSSI":
            return "MR1";
          default:
            return kpi;
        }
      case 3:
        switch (kpi){
          case "RSSI":
            return "MR1";
          case "ECNO":
            return "MR4";
          case "RSCP":
            return "MR3";
          default:
            return kpi;
        }
      case 4:
        switch (kpi){
          case "RSRP":
            return "MR1";
          case "RSRQ":
            return "MR2";
          case "SINR":
            return "MR3";
          default:
            return kpi;
        }
      default:
        break;
    }
  }

  static kpi(mr: string, technology: number): string{
    switch (mr){
      case "MR1":
        switch (technology){
          case 2:
            return "RSSI";
          case 3:
            return "RSSI";
          case 4:
            return "RSRP";
          default:
            return mr;
        }
      case "MR2":
        switch (technology){
          case 4:
            return "RSRQ";
          default:
            return mr;
        }
      case "MR3":
        switch (technology){
          case 3:
            return "RSCP";
          case 4:
            return "SINR";
          default:
            return mr;
        }
      case "MR4":
        switch (technology){
          case 3:
            return "ECNO";
          default:
            return mr;
        }
      default:
        break;
    }
  }
}

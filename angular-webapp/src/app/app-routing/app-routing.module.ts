import { NgModule } from '@angular/core';
import { RouterModule, Routes} from "@angular/router";
import { LoginComponent} from "../login/login.component";
import { LiveMonitoringComponent } from "../live-monitoring/live-monitoring.component";
import { ModulesComponent } from "../modules/modules.component";
import {AboutComponent} from "../about/about.component";
import {CustomerexperienceComponent} from "../customerexperience/customerexperience.component";
import {UserService} from "../user.service";
import {ConfigurationBrowserComponent} from "../configuration-browser/configuration-browser.component";

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full' },
  {path: 'login', component: LoginComponent},
  {path: 'live-monitoring', component: LiveMonitoringComponent, canActivate: [UserService]},
  {path: 'modules', component: ModulesComponent, canActivate: [UserService]},
  {path: 'about', component: AboutComponent, canActivate: [UserService]},
  {path: 'customer-experience', component: CustomerexperienceComponent, canActivate: [UserService]},
  {path: 'configuration-browser', component: ConfigurationBrowserComponent, canActivate: [UserService]},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }

import { Injectable, EventEmitter, Output } from '@angular/core';
declare var $: any;

@Injectable()
export class WebsocketService {

  public socket: any;
  public message: any;
  private ip: string = "www.lyticshub.com";
  // private ip: string = "127.0.0.1";
  private port: string = "1197";
  // private port: string = "8080/server";

  @Output() change: EventEmitter<any> = new EventEmitter();

  constructor() {
    this.startWebsocket();
    setInterval(()=>{
      if(!navigator.onLine || this.socket.readyState === this.socket.CLOSED || this.socket.readyState === this.socket.CLOSING || this.socket.readyState === this.socket.CONNECTING){
        this.setDisconnected();
        if(this.socket.readyState === this.socket.OPEN){
          this.socket.close();
        }
      }else if(this.socket.readyState === this.socket.OPEN){
        this.setConnected();
      }
    }, 100);
  }

  startWebsocket(){
    this.socket = new WebSocket("ws://" + this.ip + ":" + this.port);
    this.socket.onopen = (event) => {
      // this.send("Hello");
      console.log("Connected!");
      this.setConnected();
    };
    // Emit the received message to all subscribed components
    this.socket.onmessage = (event) => {
      // console.log("Received from server: " + event.data);
      try{
        var obj = JSON.parse(event.data);
        // var key = "count(MR1)";
        // console.log(obj[key]);
        // if(obj.hasOwnProperty(key)){
        // console.log("object is: " +obj[key]);
        // console.log("found it!");
        // alert("Found it!");
        // }
        this.message = obj;
      }
      catch (e){
        // console.log("NOT JSON: " + event.data);
        this.message = event.data;
      }
      this.change.emit(this.message);
    };

    this.socket.onerror = (event) => {
      // console.log("Websocket error");
      this.retryConnection();
    };

    this.socket.onclose = (event) => {
      this.retryConnection();
    };
  }

  send(data: string): void {
    this.socket.send(data);
    // console.log("Sent data!");
  }

  retryConnection(){
    this.setDisconnected();
    setTimeout(()=>{
      this.startWebsocket();
    }, 1000);
  }

  setDisconnected(){
    $('#connectedButton').html('<i class="fa fa-circle" style="color: red; font-size: 12px;"></i> Disconnected');
  }

  setConnected(){
    $('#connectedButton').html('<i class="fa fa-circle" style="color: lime; font-size: 12px;"></i> Connected');
  }

}

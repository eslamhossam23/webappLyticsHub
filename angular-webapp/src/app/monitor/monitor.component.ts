import { AfterViewInit, Component, OnInit } from '@angular/core';
import { WebsocketService } from "../websocket.service";
import {UserService} from "../user.service";
import {Time} from "../utilities/time";
import {KpitoMr} from "../utilities/kpito-mr";
import {KmlHandler} from "../utilities/kml-handler";
import {ChartHandler} from "../utilities/chart-handler";


declare var google: any;
declare var geoXML3: any;
// declare var capture0: any;
// declare var capture1: any;
declare var $: any;
declare var Chart: any;
declare var style: any;

@Component({
  selector: 'app-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.css']
})
export class MonitorComponent implements OnInit, AfterViewInit {

  public technology: number;
  public operator: string;
  public limit: Array<boolean> = [false, false, false, false, false];
  public limitMaps: Array<boolean> = [false, false, false, false, false];
  public limitCharts: Array<boolean> = [false, false, false, false, false];
  public orderMaps: Array<number> = [-1,-1,-1,-1,-1];
  public orderCharts: Array<number> = [-1,-1,-1,-1,-1];
  public mapQueries: number = -1;
  public mapQueriesCounter: number = 0;
  public chartQueries: number = -1;
  public chartQueriesCounter: number = 0;
  public mapDone: boolean = false;
  public chartDone: boolean = false;

  constructor(private userService: UserService, private websocketService: WebsocketService) { }

  ngOnInit() {
    this.operator = this.userService.getOperator();
    //Subscribe to messages coming from the WebSocket service
    this.websocketService.change.subscribe(message => {
      //message can be a JS Object so printing it may not work if concatenated with string
      if((typeof message) == "string")
      {
        // console.log("Monitor component received " + message);
      }else if((typeof message) == "object"){
        // console.log("Received obj");
        // console.log(message);
        if(!window.location.href.includes("live-monitoring")){
          return;
        }
        if(message.longitude && message.latitude){
          console.log(message);
          // console.log(message[Object.keys(message)[0]]);
          // get resolution from sensitivity and kpiName from choices
          // var kmlString = this.createKMLHeader() + this.KMLpreparePolygons(results, resolution, kpiName) + this.createKMLFooter();
          // resolution must be * 0.0001
          var sensitivity = $('#slider').slider("value");
          var kpiName = $('#kpi').val();
          // console.log(kpiName);
          KmlHandler.KML(message, sensitivity * 0.0001, kpiName, "map" + this.mapQueries, "map" + this.mapQueries + "Container", "capture" + this.mapQueries, "legend" + this.mapQueries, this.technology);
          var id = "#map" + this.mapQueries + "List";
          $(id).fadeIn();
          var mapTab = "#map" + this.mapQueries + "Tab";
          var mapContainer = "#map" + this.mapQueries + "Container";
          this.clear();
          $(mapTab).trigger('click');
          if(!$(mapContainer).hasClass("active")){
            $(mapContainer).addClass("active");
          }
          // console.log("CLICK :" + mapTab);
          var mapTabNameID = "#map" + this.mapQueries + "TabName";
          $(mapTabNameID).html('Map ' + this.mapQueriesCounter);
          this.mapDone = true;
          this.adjustContainerHeight();
        }else if(message.Date_Time){
          // console.log("Received a chart!");
          console.log(message);
          var kpiName = $('#kpi').val();
          ChartHandler.lineChart(message, kpiName, this.technology, 'chart' + this.chartQueries);
          var id = "#chart" + this.chartQueries + "List";
          $(id).fadeIn();
          var chartTab = "#chart" + this.chartQueries + "Tab";
          var chartContainer = "#chart" + this.mapQueries + "Container";
          this.clear();
          $(chartTab).trigger('click');
          if(!$(chartContainer).hasClass("active")){
            $(chartContainer).addClass("active");
          }
          var chartTabNameID = "#chart" + this.chartQueries + "TabName";
          $(chartTabNameID).html('Chart ' + this.chartQueriesCounter);
          this.chartDone = true;
          this.adjustContainerHeight();
        }
        if(this.mapDone && this.chartDone){
          this.clearUI();
        }
      }
    });

    // console.log(availableTags);

    $( "#liveMonitoringIMSI" ).autocomplete({
      source: this.userService.getIMSIs()
    });


    $('#liveMonitoringIMSIRadioButton').click(function () {
      $('#liveMonitoringIMSI').fadeIn();
    });
    $('[name="roaming"]').click(function () {
      if(this.id != "liveMonitoringIMSIRadioButton"){
        $('#liveMonitoringIMSI').fadeOut();
      }
    });
    var kpi = $('#kpi');
    kpi.selectmenu();
    $('[name="technology"]').click(function () {
      if(this.id === "2G"){
        kpi.children('option').remove();
        kpi.append('<option value="RSSI">RSSI</option>');
        kpi.val("RSSI");
      }else if(this.id === "3G"){
        kpi.children('option').remove();
        kpi.append('<option value="RSSI">RSSI</option>');
        kpi.append('<option value="ECNO">ECNO</option>');
        kpi.append('<option value="RSCP">RSCP</option>');
        kpi.val("RSSI");
      }else if(this.id === "4G"){
        kpi.children('option').remove();
        kpi.append('<option value="RSRP">RSRP</option>');
        kpi.append('<option value="RSRQ">RSRQ</option>');
        kpi.append('<option value="SINR">SINR</option>');
        kpi.val("RSRP");
      }
      kpi.append('<option value="DL">FTP Downlink</option>');
      kpi.append('<option value="UL">FTP Uplink</option>');
      kpi.append('<option value="Ping">Ping</option>');
      kpi.append('<option value="Google_P">Google Ping</option>');
      kpi.append('<option value="Facebook_P">Facebook Ping</option>');
      kpi.append('<option value="YouTube_P">YouTube Ping</option>');
      kpi.append('<option value="DL_Internet">Internet Downlink</option>');
      kpi.append('<option value="HTTP_Delay">HTTP Request Delay</option>');
      kpi.append('<option value="Google_H">Google HTTP Delay</option>');
      kpi.append('<option value="Facebook_H">Facebook HTTP Delay</option>');
      kpi.append('<option value="YouTube_H">YouTube HTTP Delay</option>');
      kpi.selectmenu("refresh");
    });
    var slider = $('#slider');
    slider.slider({
      value: 1,
      min: 1,
      range: "min",
      max: 10,
      step: 0.5,
      slide: function( event, ui ) {
        $( '#mapSensitivity').val(ui.value);
      }
    });
    $('.ui-widget-header').css('background', '#08d');
    $( '#mapSensitivity').val(slider.slider("value"));



    var chartSensitivity = $('#chartSensitivity');
    chartSensitivity.selectmenu();






    $('input[name="output"]').each(function() {
      $(this).click(function () {
        switch ($(this).val()){
          case "maps":
            $('#mapSensitivityContainer').fadeToggle();
            $('#slider').fadeToggle();
            break;
          case "charts":
            $('#chartSensitivityContainer').fadeToggle();
            break;
          default:
            break;
        }
      });
    });



    $(window).resize(() => {
      this.adjustContainerHeight();
    });


    $('#collapseButtonMonitor').click(function () {
      var monitorSideBar = $('#monitorSideBar');
      var monitorSideBarClassesExpanded = ['col-6', 'col-sm-4', 'col-lg-3'];
      var monitorSideBarClassesCollapsed = ['col-2', 'col-sm-2', 'col-lg-2'];
      var mapContainer = $('#map-container');
      var mapContainerClassesCollapsed = ['col-6', 'col-sm-8', 'col-lg-9'];
      var mapContainerClassesExpanded = ['col-10', 'col-sm-10', 'col-lg-10'];
      var arrow = $(this).find('i');
      if(monitorSideBar.hasClass(monitorSideBarClassesExpanded[0]) || monitorSideBar.hasClass(monitorSideBarClassesExpanded[1]) || monitorSideBar.hasClass(monitorSideBarClassesExpanded[2])){
        monitorSideBar.switchClass(monitorSideBarClassesExpanded[0], monitorSideBarClassesCollapsed[0], 500);
        monitorSideBar.switchClass(monitorSideBarClassesExpanded[1], monitorSideBarClassesCollapsed[1], 500);
        monitorSideBar.switchClass(monitorSideBarClassesExpanded[2], monitorSideBarClassesCollapsed[2], 500);
        mapContainer.switchClass(mapContainerClassesCollapsed[0], mapContainerClassesExpanded[0], 500);
        mapContainer.switchClass(mapContainerClassesCollapsed[1], mapContainerClassesExpanded[1], 500);
        mapContainer.switchClass(mapContainerClassesCollapsed[2], mapContainerClassesExpanded[2], 500);
        arrow.switchClass('fa-angle-double-left', 'fa-angle-double-right', 0);
      }else{
        mapContainer.switchClass(mapContainerClassesExpanded[0], mapContainerClassesCollapsed[0], 500);
        mapContainer.switchClass(mapContainerClassesExpanded[1], mapContainerClassesCollapsed[1], 500);
        mapContainer.switchClass(mapContainerClassesExpanded[2], mapContainerClassesCollapsed[2], 500);
        monitorSideBar.switchClass(monitorSideBarClassesCollapsed[0], monitorSideBarClassesExpanded[0], 500);
        monitorSideBar.switchClass(monitorSideBarClassesCollapsed[1], monitorSideBarClassesExpanded[1], 500);
        monitorSideBar.switchClass(monitorSideBarClassesCollapsed[2], monitorSideBarClassesExpanded[2], 500);
        arrow.switchClass('fa-angle-double-right', 'fa-angle-double-left', 0);
      }
    });
  }

  onMapsReady(){
    for(var i = 0; i < this.limitMaps.length; i++){
      var id = 'map' + i;
      var uluru = {lat: 30, lng: 31};
      var map = new google.maps.Map(document.getElementById(id), {
        zoom: 15,
        center: uluru,
        keyboardShortcuts: false,
        streetViewControl: false,
        fullscreenControl: false,
        zoomControl: false,
        zoomControlOptions: false,
        mapTypeId: 'roadmap'
      });
    }

  }


  ngAfterViewInit() : void{
    Time.initTime("startDateMonitor", "endDateMonitor", "startTimeMonitor", "endTimeMonitor");


    $('.tabName').on("keydown", function (e) {
      if (e.which == 13) {
        //Prevent insertion of a return
        //You could do other things here, for example
        //focus on the next field
        return false;
      }
    });
    $(".ui-button").css("background", "linear-gradient(white, gray)");
    $(".ui-selectmenu-button").css("background", "linear-gradient(white, gray)");
    $(".ui-selectmenu-button-closed").css("background", "linear-gradient(white, gray)");

    // this.adjustCollapseButtonHeight();
    this.adjustCollapseButtonWidth();
  }






  // results is the received query result, resolution is sensitivity * 0.0001, kpiName is the selection after the technology



  //Set technology parameter
  onTechnologySelect(entry): void {
    this.technology = entry;
  }






  //Function to be used to send queries
  sendQuery(): void{
    if(!this.checkInput()){
      alert("Please make sure to fill all criteria.");
      return;
    }

    var query;
    var maps = false;
    var charts = false;

    $('input[name="output"]:checked').each(function() {
      switch (this.value){
        case "maps":
          maps = true;
          break;
        case "charts":
          charts = true;
          break;
        default:
          break;
      }
    });

    if(maps){
      query = this.createMapQuery();
      var mapContainer = '#map' + this.mapQueries + 'Container';
      $(mapContainer).removeClass("active");
      this.mapQueries = (this.mapQueries + 1) % this.limitMaps.length;
      this.mapQueriesCounter++;
      var full = true;
      var oldestQuery = 0;
      for(var i = 0; i < this.limit.length; i++){
        if(!this.limitMaps[i]){
          full = false;
          break;
        }
      }
      if(full){
        for(var i = 0; i < this.limit.length; i++){
          if(this.orderMaps[oldestQuery] > this.orderMaps[i]){
            oldestQuery = i;
          }
        }
        this.mapQueries = oldestQuery;
      }else {
        for(var i = 0; i < this.limit.length; i++){
          if(!this.limitMaps[i]){
            this.mapQueries = i;
            break;
          }
        }
      }
      // for(var i = 0; i < this.limit.length; i++){
      //   if(!this.limit[i]){
      //      break;
      //   }
      // }
      this.limitMaps[this.mapQueries] = true;
      this.orderMaps[this.mapQueries] = this.mapQueriesCounter;
      console.log(JSON.stringify(query));
      this.websocketService.send(JSON.stringify(query));
      this.mapDone = false;
      this.chartDone = true;
    }
    if(charts){
      query = this.createChartQuery();
      var chartContainer = '#chart' + this.mapQueries + 'Container';
      $(chartContainer).removeClass("active");
      this.chartQueries = (this.chartQueries + 1) % this.limitCharts.length;
      this.chartQueriesCounter++;
      var full = true;
      var oldestQuery = 0;
      for(var i = 0; i < this.limit.length; i++){
        if(!this.limitCharts[i]){
          full = false;
          break;
        }
      }
      if(full){
        for(var i = 0; i < this.limit.length; i++){
          if(this.orderCharts[oldestQuery] > this.orderCharts[i]){
            oldestQuery = i;
          }
        }
        this.chartQueries = oldestQuery;
      }else {
        for(var i = 0; i < this.limit.length; i++){
          if(!this.limitCharts[i]){
            this.chartQueries = i;
            break;
          }
        }
      }

      this.limitCharts[this.chartQueries] = true;
      this.orderCharts[this.chartQueries] = this.chartQueriesCounter;
      console.log(JSON.stringify(query));
      this.websocketService.send(JSON.stringify(query));
      this.mapDone = true;
      this.chartDone = false;
    }
    if(maps && charts){
      this.mapDone = false;
      this.chartDone = false;
    }
    if(maps || charts){
      var queryButton = $("#monitorQueryButton");
      queryButton.html('<i class="fa fa-spinner fa-spin" style="color: #1F415D; font-size: 18px;"></i>');
      queryButton.prop('disabled', true);
      // $('#loadingModal').modal('show', {keyboard: false, backdrop: 'static'});
    }

  }


  createChartQuery(): any{
    // Chart query
    // {query=[[basic], [MR1, Date_Time], [all, timeavg-8], [IMSI, technology, Date_Time], [=60204%, =4, 20180324201427->20180403201427]]}
    var queryType = this.queryType();
    var kpiName = $('#kpi').val();
    var mr = KpitoMr.mr(kpiName, this.technology);
    // console.log("mr is: " + mr);
    var dateTime = "Date_Time";
    var imsi = this.imsi();
    var techno = "=" + this.technology;
    var chartSensitivity = $('#chartSensitivity').val();
    var sensitivity;
    switch (chartSensitivity){
      case "Daily":
        sensitivity = 8;
        break;
      case "Hourly":
        sensitivity = 10;
        break;
      default:
        break;
    }

    var dateTimeFormat = Time.dateFormat("startDateMonitor", "endDateMonitor", "startTimeMonitor", "endTimeMonitor");

    var indoor = (<HTMLInputElement>document.getElementById('indoorSwitch')).checked;
    var query;
    if(indoor){
      query = {"query": [queryType, [mr, dateTime], ["all", "timeavg-" + sensitivity], ["IMSI","technology", "Date_Time", "accuracy"], [imsi, techno, dateTimeFormat[0] + "-\u003e" + dateTimeFormat[1], 30 + "-\u003e" + 10000 ]]};
      // query = {"query": [queryType, [mr, dateTime], ["all", "timeavg-" + sensitivity], ["IMSI", "Date_Time", "accuracy"], [imsi, dateTimeFormat[0] + "-\u003e" + dateTimeFormat[1], 30 + "-\u003e" + 10000 ]]};
    }else{
      query = {"query": [queryType, [mr, dateTime], ["all", "timeavg-" + sensitivity], ["IMSI","technology", "Date_Time"], [imsi, techno, dateTimeFormat[0] + "-\u003e" + dateTimeFormat[1] ]]};
      // query = {"query": [queryType, [mr, dateTime], ["all", "timeavg-" + sensitivity], ["IMSI", "Date_Time"], [imsi, dateTimeFormat[0] + "-\u003e" + dateTimeFormat[1] ]]};
    }
    return query;
  }

  createMapQuery(): any{
    var queryType = this.queryType();
    var colNames = ["IMSI", "technology", "PLMN", "Date_Time"];
    var operators = ["60201", "60202", "60203", "60204"];
    var plmn = "=";
    var techno = "=" + this.technology;
    var imsi = this.imsi();
    var kpiName = $('#kpi').val();
    var mr = KpitoMr.mr(kpiName, this.technology);
    var sensitivity = $('#slider').slider("value");

    switch ($('input[name=roaming]:checked').val()){
      // Roaming on: remove PLMN
      case "on":
        colNames.splice(2, 1);
        break;
      // Roaming off: PLMN = operator
      case "off":
        plmn += this.operator;
        // console.log(plmn);
        break;
      case "nationalRoaming":
        operators.splice(operators.indexOf(this.operator), 1);
        var i;
        plmn = "(";
        for(i = 0; i < operators.length; i++){
          plmn += operators[i];
          if(i != operators.length - 1){
            plmn += ",";
          }
        }
        plmn += ")";
        // console.log(operators);
        // console.log(plmn);
        break;
      case "IMSI":
        colNames.splice(2, 1);
        break;
      default:
        // console.log("skip roaming");
        break;
    }
    // console.log(imsi);



    // console.log(mr);

    // console.log(sensitivity);
    sensitivity *= 0.0001;

    var dateTime = Time.dateFormat("startDateMonitor", "endDateMonitor", "startTimeMonitor", "endTimeMonitor");

    var indoor = (<HTMLInputElement>document.getElementById('indoorSwitch')).checked;
    var query;
    if(indoor){
      if(plmn.length > 1){
        query = {"query": [queryType, [mr, "latitude", "longitude"], ["all", "locationavg-" + sensitivity, "locationavg-" + sensitivity], ["IMSI", "PLMN", "technology", "Date_Time", "accuracy"], [imsi, plmn, techno, dateTime[0] + "-\u003e" + dateTime[1], 30 + "-\u003e" + 10000 ]]} ;
        // query = {"query": [queryType, [mr, "latitude", "longitude"], ["all", "locationavg-" + sensitivity, "locationavg-" + sensitivity], ["IMSI", "PLMN", "Date_Time", "accuracy"], [imsi, plmn, dateTime[0] + "-\u003e" + dateTime[1], 30 + "-\u003e" + 10000 ]]} ;
      }else{
        query = {"query": [queryType, [mr, "latitude", "longitude"], ["all", "locationavg-" + sensitivity, "locationavg-" + sensitivity], ["IMSI", "technology", "Date_Time", "accuracy"], [imsi, techno, dateTime[0] + "-\u003e" + dateTime[1], 30 + "-\u003e" + 10000 ]]} ;
        // query = {"query": [queryType, [mr, "latitude", "longitude"], ["all", "locationavg-" + sensitivity, "locationavg-" + sensitivity], ["IMSI", "Date_Time", "accuracy"], [imsi, dateTime[0] + "-\u003e" + dateTime[1], 30 + "-\u003e" + 10000 ]]} ;
      }
    }else if(plmn.length > 1){
      query = {"query": [queryType, [mr, "latitude", "longitude"], ["all", "locationavg-" + sensitivity, "locationavg-" + sensitivity], ["IMSI", "PLMN", "technology", "Date_Time"], [imsi, plmn, techno, dateTime[0] + "-\u003e" + dateTime[1] ]]} ;
      // query = {"query": [queryType, [mr, "latitude", "longitude"], ["all", "locationavg-" + sensitivity, "locationavg-" + sensitivity], ["IMSI", "PLMN", "Date_Time"], [imsi, plmn, dateTime[0] + "-\u003e" + dateTime[1] ]]} ;
    }else{
      query = {"query": [queryType, [mr, "latitude", "longitude"], ["all", "locationavg-" + sensitivity, "locationavg-" + sensitivity], ["IMSI", "technology", "Date_Time"], [imsi, techno, dateTime[0] + "-\u003e" + dateTime[1] ]]} ;
      // query = {"query": [queryType, [mr, "latitude", "longitude"], ["all", "locationavg-" + sensitivity, "locationavg-" + sensitivity], ["IMSI",  "Date_Time"], [imsi, startDateString + "-\u003e" + endDateString ]]} ;
    }



    // console.log(query);


    return query;
  }


  queryType(): Array<string> {
    return !this.speedTest() ? ["basic"] : ["speedtest"];
  }

  speedTest(): boolean {
    var kpi = $('#kpi').val();
    // console.log(kpi);
    // console.log((kpi != "RSRP") && (kpi != "RSRQ") && (kpi != "ECNO") && (kpi != "RSCP") && (kpi != "SINR") && (kpi != "RSSI"));
    return (kpi != "RSRP") && (kpi != "RSRQ") && (kpi != "ECNO") && (kpi != "RSCP") && (kpi != "SINR") && (kpi != "RSSI");
  }

  imsi(): string{
    switch ($('input[name=roaming]:checked').val()){
      case "IMSI":
        return "=" + $('#liveMonitoringIMSI').val();
      default:
        return "=" + this.operator + "%";
    }
  }

  clearUI() {
    // $('#loadingModal').modal('hide');
    var queryButton = $("#monitorQueryButton");
    queryButton.html('Run');
    queryButton.prop('disabled', false);

  }



  close(event, index, type): void{
    event.stopPropagation();
    switch (type){
      case "map":
        this.limitMaps[index] = false;
        break;
      case "chart":
        this.limitCharts[index] = false;
        break;
      default:
        break;
    }
    var listID = "#" + type + index + "List";
    var tabID = "#" + type + index + "Tab";
    var containerID = "#" + type + index + "Container";
    $(listID).hide();
    $(listID).removeClass("active");
    $(tabID).removeClass("active");
    $(containerID).removeClass("active");
  }

  adjustContainerHeight(){
    var height = $('#map-container').height() - $('#tabBarLiveMonitoring').height();
    $('#bigContainer').height(height);
  }


  adjustCollapseButtonHeight(){
    var cards = $('#accordion').find('.card');
    var height = 0;
    for(var i = 0; i < cards.length; i++){
      height = height + $(cards[i]).height();
    }
    height = height + $('#runButtonContainer').height();
    $('#collapseButtonMonitor').height(height);
  }

  adjustCollapseButtonWidth(){
    var button = $('#collapseButtonMonitor');
    var container = $('#collapseButtonDivMonitor');
    container.css("max-width", button.width()*2);
  }

  checkInput(): boolean{
    if(typeof $('input[name=roaming]:checked').val() === 'undefined'){
      return false;
    }else if(typeof $('input[name=technology]:checked').val() === 'undefined'){
      return false;
    }

    return true;
  }


  clear(){
    for(var i = 0; i < this.limit.length; i++){
      var mapListID = "#map"  + i + "List";
      var mapTabID = "#map"  + i + "Tab";
      var mapContainerID = "#map" + i + "Container";
      var chartListID = "#chart"  + i + "List";
      var chartTabID = "#chart"  + i + "Tab";
      var chartContainerID = "#chart" + i + "Container";
      $(mapListID).removeClass("active");
      $(mapTabID).removeClass("active");
      $(mapContainerID).removeClass("active");
      $(chartListID).removeClass("active");
      $(chartTabID).removeClass("active");
      $(chartContainerID).removeClass("active")
    }
  }


}

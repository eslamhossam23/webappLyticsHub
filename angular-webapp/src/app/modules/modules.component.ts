import { Component, OnInit } from '@angular/core';
import {UserService} from "../user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css']
})
export class ModulesComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    // this.userService.verifyLogin();
    // if(!this.userService.getLoggedIn()){
    //   this.router.navigateByUrl('/login');
    // }
  }

  alert(){
    alert("You do not have permission to see this module.");
  }


}

import { Component, OnInit, AfterViewInit } from '@angular/core';
import {UserService} from "../user.service";
import {WebsocketService} from "../websocket.service";
import { saveAs } from 'file-saver';

declare var google: any;
declare var $: any;
declare var XLSX: any;

@Component({
  selector: 'app-active-test',
  templateUrl: './active-test.component.html',
  styleUrls: ['./active-test.component.css']
})
export class ActiveTestComponent implements OnInit, AfterViewInit {

  private counter = 0;
  private counterIMSI = [];
  private imsiList = [];



  constructor(private userService: UserService, private websocketService: WebsocketService) { }

  ngOnInit() {
    $( "#imsiLiveStatusCheck" ).autocomplete({
      source: this.userService.getIMSIs()
    });


    this.websocketService.change.subscribe(message => {
      if((typeof message) == "object"){
        //  {Network Type=MOBILE[LTE], APN/WIFI=te.mobile, IP=154.185.177.162, Battery=92, IMSI=602040000101353, Long=29.59544805, IMEI=867296028237838, Private IP=fe80::14eb:12aa:e059:2239%rmnet_data0, Lat=30.9980795, Datetime=20180408121824}
        // console.log(message);
        if(message.initialize){
          setTimeout(()=>{
            $( "#imsiLiveStatusCheck" ).autocomplete({
              source: this.userService.getIMSIs()
            });
            $('#imsiLiveTesting').multipleSelect(
              {
                placeholder: "Please select an IMSI"
              }
            );
            $('#imsiLiveTesting').children("option").remove();
            var options = this.userService.getIMSIs();
            for(var i = 0; i < options.length; i++){
              $('<option />', {value: options[i], text: options[i]}).appendTo("#imsiLiveTesting");
            }
            $('#imsiLiveTesting').multipleSelect('refresh');
          },1000);
        }else if(message.Lat && message.Long){
          if(parseInt(message.Lat) == 0){
            alert("Your query was ignored because current lat and long are zeroes.");
            return;
          }
          console.log(message);

          var uluru = {lat: parseFloat(message.Lat), lng: parseFloat(message.Long)};
          var map = new google.maps.Map(document.getElementById('mapActiveTest'), {
            zoom: 16,
            center: uluru,
            keyboardShortcuts: false,
            streetViewControl: false,
            fullscreenControl: false,
            zoomControl: false,
            zoomControlOptions: false,
            mapTypeId: 'roadmap'
          });

          // var date;
          // date.replace(/\//g , "");
          // // console.log("Converted date: " + date);
          // var day = date.substr(6, 2);
          // var month = date.substr(4, 2);
          // var year = date.substr(0, 4);
          // var fullDate = day + "/" + month + "/" + year;
          // var time = date.substr(6, 2) + "/" + date.substr(4, 2) + "/" + date.substr(0, 4) + " "  + date.substr(8, 2) + ":" + date.substr(10, 2) + ":" + date.substr(12, 2);

          var date = message.Datetime;
          var time = date.substr(6, 2) + "/" + date.substr(4, 2) + "/" + date.substr(0, 4) + " "  + date.substr(8, 2) + ":" + date.substr(10, 2) + ":" + date.substr(12, 2);

          // var time = "2001";
          var imsi = message.IMSI;
          var imei = message.IMEI;
          var battery = message.Battery;
          var ip = message.IP;
          var networkType = message.Network_Type;
          var apnWifi = message.APN_WIFI;

          var contentString = '<div id=\"content\">' +
            '<div id=\"siteNotice\">' +
            '</div>' +
            '<h3 id=\"firstHeading\" class=\"firstHeading\">Last User Info</h3>' +
            '<div id=\"bodyContent\">' +
            '<p><span style=\"font-weight: bold;\">Last Update Time : </span>' + time + '</p>' +
            '<p><span style=\"font-weight: bold;\">IMSI : </span>' + imsi + '</p>'  +
            '<p><span style=\"font-weight: bold;\">IMEI : </span>' +  imei + '</p>'  +
            '<p><span style=\"font-weight: bold;\">Battery Status : </span>' + battery + '%</p>'  +
            '<p><span style=\"font-weight: bold;\">IP Address : </span>' + ip + '</p>'  +
            '<p><span style=\"font-weight: bold;\">Network Type : </span>' + networkType + '</p>'  +
            '<p><span style=\"font-weight: bold;\">APN/WIFI : </span>'+ apnWifi + '</p>'  +
            '</div>'  +
            '</div>';
          var infowindow = new google.maps.InfoWindow({
            content: contentString
          });

          var marker = new google.maps.Marker({
            position: uluru,
            map: map,
            title: 'User'
          });
          marker.addListener('click', function() {
            infowindow.open(map, marker);
          });
        }else if(message.Ping){
          var row;
          var couple;
          var coupleIndex;
          for(var i = 0; i < this.counterIMSI.length; i++){
            couple = this.counterIMSI[i];
            if(couple[1] === message.IMSI){
              row = couple[0];
              coupleIndex = i;
            }
          }
          this.editRow(row, message.IMSI, "Done", message.DL_Speedtest, message.UL_Speedtest, message.Ping, message.HTTP_Download, message.HTTP_Delay);
          this.counterIMSI[coupleIndex] = [-1,-1];
        }
      }
    });



    this.readXLSX();

    $('#collapseButtonActiveTest').click(function () {
      var activeTestSideBar = $('#activeTestSideBar');
      var activeTestSideBarClassesExpanded = ['col-4', 'col-sm-4', 'col-lg-3'];
      var activeTestSideBarClassesCollapsed = ['col-2', 'col-sm-2', 'col-lg-2'];
      var activeTestContainer = $('#activeTestContainer');
      var activeTestContainerClassesCollapsed = ['col-8', 'col-sm-8', 'col-lg-9'];
      var activeTestContainerClassesExpanded = ['col-10', 'col-sm-10', 'col-lg-10'];
      var arrow = $(this).find('i');
      if(activeTestSideBar.hasClass(activeTestSideBarClassesExpanded[0]) || activeTestSideBar.hasClass(activeTestSideBarClassesExpanded[1]) || activeTestSideBar.hasClass(activeTestSideBarClassesExpanded[2])){
        activeTestSideBar.switchClass(activeTestSideBarClassesExpanded[0], activeTestSideBarClassesCollapsed[0], 500);
        activeTestSideBar.switchClass(activeTestSideBarClassesExpanded[1], activeTestSideBarClassesCollapsed[1], 500);
        activeTestSideBar.switchClass(activeTestSideBarClassesExpanded[2], activeTestSideBarClassesCollapsed[2], 500);
        activeTestContainer.switchClass(activeTestContainerClassesCollapsed[0], activeTestContainerClassesExpanded[0], 500);
        activeTestContainer.switchClass(activeTestContainerClassesCollapsed[1], activeTestContainerClassesExpanded[1], 500);
        activeTestContainer.switchClass(activeTestContainerClassesCollapsed[2], activeTestContainerClassesExpanded[2], 500);
        arrow.switchClass('fa-angle-double-left', 'fa-angle-double-right', 0);
      }else{
        activeTestContainer.switchClass(activeTestContainerClassesExpanded[0], activeTestContainerClassesCollapsed[0], 500);
        activeTestContainer.switchClass(activeTestContainerClassesExpanded[1], activeTestContainerClassesCollapsed[1], 500);
        activeTestContainer.switchClass(activeTestContainerClassesExpanded[2], activeTestContainerClassesCollapsed[2], 500);
        activeTestSideBar.switchClass(activeTestSideBarClassesCollapsed[0], activeTestSideBarClassesExpanded[0], 500);
        activeTestSideBar.switchClass(activeTestSideBarClassesCollapsed[1], activeTestSideBarClassesExpanded[1], 500);
        activeTestSideBar.switchClass(activeTestSideBarClassesCollapsed[2], activeTestSideBarClassesExpanded[2], 500);
        arrow.switchClass('fa-angle-double-right', 'fa-angle-double-left', 0);
      }
    });

    $('input[name=livetesting]').click(function () {

      // console.log($(this).val());

      if($(this).val() == "imsiSelect"){
        $('#imsiMultipleSelectContainer').show();
        $('#imsisFileButtons').hide();
      }else if($(this).val() == "imsiFile"){
        $('#imsiMultipleSelectContainer').hide();
        $('#imsisFileButtons').show();
      }
    });
  }

  ngAfterViewInit(){
    this.initialiseTable();
    setTimeout(()=>{
      $( "#imsiLiveStatusCheck" ).autocomplete({
        source: this.userService.getIMSIs()
      });
      $('#imsiLiveTesting').multipleSelect(
        {
          placeholder: "Please select an IMSI"
        }
      );
      $('#imsiLiveTesting').children("option").remove();
      var options = this.userService.getIMSIs();
      for(var i = 0; i < options.length; i++){
        $('<option />', {value: options[i], text: options[i]}).appendTo("#imsiLiveTesting");
      }
      $('#imsiLiveTesting').multipleSelect('refresh');
    },1000);
  }

  onMapsReady(){
    var id = 'mapActiveTest';
    var uluru = {lat: 30, lng: 31};
    var map = new google.maps.Map(document.getElementById(id), {
      zoom: 15,
      center: uluru,
      keyboardShortcuts: false,
      streetViewControl: false,
      fullscreenControl: false,
      zoomControl: false,
      zoomControlOptions: false,
      mapTypeId: 'roadmap'
    });
  }

  sendQuery(){
    // {Ping_Num = 100, HTTP_URL = www.facebook.com, HTTP_Download = true, Test = true, Ping = true, HTTP_Delay = true, IMSI = imsi, UL_Speedtest = true, DL_Speedtest = true, Ping_Add = www.google.com }
    var pingNumber = $("#pingNumber").val();
    var imsi;
    var httpDownload = (<HTMLInputElement>document.getElementById('httpDownload')).checked;
    var ping = (<HTMLInputElement>document.getElementById('pingActiveTest')).checked;
    var pingAddr;
    if(ping){
      pingAddr = $("#pingAddr").val();
    }else{
      pingAddr = "null";
    }
    var httpDelay = (<HTMLInputElement>document.getElementById('httpDelay')).checked;
    var httpURL;
    if(httpDelay){
      httpURL = $('#httpURL').val();
    }else{
      httpURL = "null";
    }
    var uplinkSpeed = (<HTMLInputElement>document.getElementById('uplinkSpeed')).checked;
    var downlinkSpeed = (<HTMLInputElement>document.getElementById('downlinkSpeed')).checked;
    // query = {"query": [queryType, [mr, dateTime], ["all", "timeavg-" + sensitivity], ["IMSI","technology", "Date_Time"], [imsi, techno, startDateString + "-\u003e" + endDateString ]]};

    // {HTTP_Download=false, Test=true, Ping=false, HTTP_Delay=false, IMSI=602030950384765, UL_Speedtest=false, DL_Speedtest=true} first send
    // reply {Ping_Num=null, HTTP_URL=null, HTTP_Download=false, Test=true, Ping=false, HTTP_Delay=false, IMSI=602030950384765, UL_Speedtest=false, DL_Speedtest=true, Ping_Add=null}

    var imsiList = [];
    if($('input[name=livetesting]:checked').val() === "imsiSelect"){
      imsiList = $('#imsiLiveTesting').multipleSelect('getSelects');
    }else{
      imsiList = this.imsiList;
    }
    if(imsiList.length == 0){
      alert("Please choose at least one IMSI.");
      return;
    }
    console.log(imsiList);
    for(var i = 0; i < imsiList.length; i++){
      imsi = imsiList[i];
      var query = {"Active_test": [[ "IMSI", "Test", "Ping_Num", "HTTP_URL", "HTTP_Download", "Ping", "HTTP_Delay", "UL_Speedtest", "DL_Speedtest", "Ping_Add"], [imsi.toString(), "true", pingNumber.toString(), httpURL.toString(), httpDownload.toString(),  ping.toString(), httpDelay.toString(), uplinkSpeed.toString(), downlinkSpeed.toString(), pingAddr.toString()] ]};
      this.editRow(this.counter, imsi, "Running...", "", "", "", "", "");
      this.counterIMSI.push([this.counter, imsi]);
      this.counter++;
      this.websocketService.send(JSON.stringify(query));
      console.log(query);
    }
    // console.log(this.counterIMSI);










    // this.editRow(1, "Waiting", 123, 123, 123, 123, 123, 123);
    // this.editRow(2, "Waiting", 123, 123, 123, 123, 123, 123);
    // this.editRow(3, "Waiting", 123, 123, 123, 123, 123, 123);
    // this.editRow(4, "Waiting", 123, 123, 123, 123, 123, 123);
    // this.editRow(5, "Waiting", 123, 123, 123, 123, 123, 123);
    // this.editRow(6, "Waiting", 123, 123, 123, 123, 123, 123);
    // this.editRow(7, "Waiting", 123, 123, 123, 123, 123, 123);
    // this.editRow(8, "Waiting", 123, 123, 123, 123, 123, 123);
    // this.editRow(9, "Waiting", 123, 123, 123, 123, 123, 123);
    // this.editRow(10, "Waiting", 123, 123, 123, 123, 123, 123);
    // this.editRow(11, "Waiting", 123, 123, 123, 123, 123, 123);
    // this.editRow(12, "Waiting", 123, 123, 123, 123, 123, 123);
    // this.editRow(13, "Waiting", 123, 123, 123, 123, 123, 123);
  }

  editRow(i, imsi, status, ftpDL, ftpUL, pingDelay, httpsDL, httpsDelay){
    $('#imsi' + i).html("<td>"+ imsi +"</td><td>" + status + "</td>" + "</td><td>" + ftpDL + "</td><td>" + ftpUL + "</td><td>" + pingDelay + "</td><td>" + httpsDL + "</td><td>" + httpsDelay + "</td>");
    if($('#imsi' + (i+1)).length < 1){
      $('#imsiTable').append('<tr id=\"imsi'+(i+1)+'\" style=\"background-color: white;\"></tr>');
    }
  }

  clearTable(){
    $('#tableBody').html('<tr id=\"imsi0\" style=\"background-color: white;\"></tr>');
    this.initialiseTable();
  }

  initialiseTable(){
    this.editRow(0, "", "", "", "", "No content in table", "", "");
    this.counter = 0;
  }

  mockData(index){
    var message;
    if(index == 1){
      message = {
        IMSI: "602040000101358",
        DL_Speedtest: "4",
        UL_Speedtest: "5",
        Ping: "9",
        HTTP_Download: "20",
        HTTP_Delay: "900"
      };
    }else {
      message = {
        IMSI: "602040000101353",
        DL_Speedtest: "20",
        UL_Speedtest: "1",
        Ping: "100",
        HTTP_Download: "10",
        HTTP_Delay: "1000"
      };
    }


    console.log("Counter IMSI on mock: ");
    console.log(this.counterIMSI);

    var row;
    var couple;
    var coupleIndex;
    for(var i = 0; i < this.counterIMSI.length; i++){
      couple = this.counterIMSI[i];
      console.log(couple);
      if(couple[1] === message.IMSI){
        row = couple[0];
        coupleIndex = i;
      }
    }
    this.editRow(row, message.IMSI, "Done", message.DL_Speedtest, message.UL_Speedtest, message.Ping, message.HTTP_Download, message.HTTP_Delay);
    this.counterIMSI[coupleIndex] = [-1,-1];
    console.log("Couple index: ");
    console.log(coupleIndex);
    console.log("Counter IMSI: ");
    console.log(this.counterIMSI);
  }


  downloadTemplate(){
    // {t: "s", v: "IMSIs", r: "<t>IMSIs</t>", h: "IMSIs", w: "IMSIs"}
    // A2
    //   :
    // {t: "n", v: 602040000101353, w: "6.0204E+14"}
    var workbook = XLSX.utils.book_new();
    workbook.SheetNames.push("Sheet1");
    var ws_data = [['IMSIs']];
    var imsiList = this.userService.getIMSIs();
    for(var i = 0; i < imsiList.length; i++){
      ws_data.push([imsiList[i]]);
    }
    var ws = XLSX.utils.aoa_to_sheet(ws_data);
    workbook.Sheets["Sheet1"] = ws;
    var wopts = { bookType:'xlsx', bookSST:false, type:'binary' };
    var wbout = XLSX.write(workbook,wopts);
    function s2ab(s) {
      var buf = new ArrayBuffer(s.length);
      var view = new Uint8Array(buf);
      for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }
    /* the saveAs call downloads a file on the local machine */
    saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), "template.xlsx");
  }


  readXLSX() {
    var fileInput = (<HTMLInputElement>document.getElementById('imsiFileInput'));

    fileInput.addEventListener('change', (e) => {
      var file = fileInput.files[0];

      var reader = new FileReader();

      reader.onload = (e) => {
        var data = reader.result;
        var workbook = XLSX.read(data, {type: 'binary'});
        // console.log(workbook);
        workbook = XLSX.read(data, {type: 'binary'});
        var worksheet = workbook.Sheets['Sheet1'];
        this.imsiList = [];
        for(var k in worksheet){
          if(worksheet.hasOwnProperty(k) && k.toString() != "!ref" && k.toString() != "A1" && k.toString() != "!margins"){
            this.imsiList.push(worksheet[k].v);
            // console.log(worksheet[k].v);
          }
        }
        // var desiredCell = worksheet['A2'];
        // console.log(desiredCell.v);
      };
      reader.readAsBinaryString(file);
    });
  }

  liveStatusCheck(){

    var imsi = $('#imsiLiveStatusCheck').val();
    var query = {"Active_test": [["IMSI", "Test"], [imsi, "false"]]};

    this.websocketService.send(JSON.stringify(query));
  }


  adjustCollapseButtonWidth(){
    var button = $('#collapseButtonActiveTest');
    var container = $('#collapseButtonDivActiveTest');
    container.css("max-width", button.width()*2);
  }

  adjustCollapseButtonHeight(){
    setTimeout(function () {
      var cards = $('#accordion2').find('.card');
      console.log(cards);
      var height = 0;
      for(var i = 0; i < cards.length; i++){
        height = height + $(cards[i]).height();
      }
      console.log(height);
      $('#collapseButtonActiveTest').height(height);
    }, 200);

  }
}

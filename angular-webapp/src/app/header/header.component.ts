import { Component, OnInit } from '@angular/core';
import {UserService} from "../user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public username: string;


  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.getUsername();
  }

  getUsername(){
    this.username = this.userService.getUsername();
  }

  goToAbout(){
    this.router.navigate(['/about']);
  }

  logOut(){
    this.userService.logOut();
  }


}

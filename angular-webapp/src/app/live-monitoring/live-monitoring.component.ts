import {AfterViewInit, Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {UserService} from "../user.service";
import {Router} from "@angular/router";
import {MonitorComponent} from "../monitor/monitor.component";
import {ActiveTestComponent} from "../active-test/active-test.component";

declare var $: any;

@Component({
  selector: 'app-live-monitoring',
  templateUrl: './live-monitoring.component.html',
  styleUrls: ['./live-monitoring.component.css']
})
export class LiveMonitoringComponent implements OnInit, AfterViewInit {

  @ViewChild(MonitorComponent)
  private monitorComponent: MonitorComponent;
  @ViewChild(ActiveTestComponent)
  private activeTestComponent: ActiveTestComponent;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(){
    // this.userService.verifyLogin();
    // if(!this.userService.getLoggedIn()){
    //   this.router.navigateByUrl('/login');
    // }

    this.adjustContainerHeight();
    // console.log("Document height:" + $(document).height());
    // console.log("HeaderLiveMonitoring height: " + $('#headerLiveMonitoring').height());

    // console.log($('#myTabContent').height());
    $(window).resize(() => {
      this.adjustContainerHeight();
    });

    var collapsibleLink = $('.card-header');
    collapsibleLink.click(function (event) {
      // Arrow bug fix
      if (event.target !== this){
        $(this).trigger('click');
        return;
      }
      var parent = $(this).parent().parent();
      var clicked = $(this).children('h5').children('h4').children('span').hasClass("fa-angle-double-up");
      collapsibleLink.each( function (index) {
        if(!$(this).parent().parent().is(parent)){
          return;
        }
        var element = $(this).children('h5');
        element.children('h4').children('span').remove();
        $(this).children('h5').children('h4').prepend('<span class="fa fa-angle-double-down" style="font-size: 18px;"></span>');
      });
      var tempHTML = $(this).innerHTML;
      $(this).innerHTML = "";
      var element = $(this).children('h5').children('h4');
      if(clicked){
        element.children('span').remove();
        element.prepend('<span class="fa fa-angle-double-down" style="font-size: 18px;"></span>');
      }else{
        element.children('span').remove();
        element.prepend('<span class="fa fa-angle-double-up" style="font-size: 18px;"></span>');
      }
      $(this).innerHTML = tempHTML;
    });

    // $('#activeTest-tab').click(() => {
    //   this.activeTestComponent.adjustCollapseButtonHeight();
    //   this.activeTestComponent.adjustCollapseButtonWidth();
    // });
  }
  ngAfterViewInit(){
    // (<any>window).googleMapsReady=this.onMapsReady.bind(this);
    // var script = document.createElement("script");
    // script.type = "text/javascript";
    // document.getElementsByTagName("head")[0].appendChild(script);
    // script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDA9vW6Uw02UZUjoscCFa-7BBzjyN4UX7E&callback=googleMapsReady";
    this.onMapsReady();
  }

  onMapsReady(){
    this.monitorComponent.onMapsReady();
    this.activeTestComponent.onMapsReady();
  }

  adjustContainerHeight(){
    var height = $(document).height() - $('#headerLiveMonitoring').height();
    $('#myTabContent').height(height);
    $('#activeTest').height(height);
  }


}
